# Từ Lenin đến Castro: Vai trò của cá nhân trong quá trình làm nên lịch sử {#Lenin-Castro-vai-tro-ca-nhan-trong-lich-su}
\chaptermark{Từ Lenin đến Castro}

Chương ba của quyển sách _Nhà tiên tri bị ruồng bỏ_ (_The Prophet Outcast_),
đây là quyển cuối cùng trong bộ tiểu sử về Trotsky\index{Trotsky}, đề cập đến nội 
dung "Nhà cách mạng với vai trò là nhà sử học", Issac Deutscher\index{Issac Deutscher} 
tranh luận về vai trò của cá nhân quyết định tới các sự kiện lịch sử một cách rất sư phạm. 
Vấn đề nêu ra ở đây là Trotsky ca ngợi vai trò của Lenin\index{Lenin} trong 
Cách mạng Nga.

Deutscher\index{Deutscher} cho rằng Trotsky dao động giữa hai quan điểm không 
thể hòa hợp. Trong tác phẩm _Lịch sử cách mạng Nga_, bức thư gửi Preobrazhensky 
vào năm 1928, và trong _Nhật ký tù đày_, Trotsky\index{Trotsky} kiên định rằng 
Lenin\index{Lenin} là không thể thiếu được cho thắng lợi của Cách mạng Tháng Mười. 
Cách mạng không thể thành công mà không có Lenin. Ở đâu đó trong 
_Cuộc cách mạng bị phản bội_ (_The Revolution Betrayed_), Deutscher cho rằng, 
Trotsky\index{Trotsky} quay lại quan điểm chính thống về duy vật lịch sử coi phẩm 
chất lãnh đạo là thứ yếu so với những nhân tố khách quan trong quá trình làm nên 
lịch sử. Liệu đó có phải là sự dao động của Trotsky\index{Trotsky}? 

Chủ nghĩa Marx cho rằng không một cá nhân nào, dù tài năng đến đâu, ý chí vững 
vàng đến đâu, hoặc chiếm vị trí chiến lược đến đâu có thể thay đổi tiến trình chủ 
đạo của diễn biến lịch sử, cái được định hình bởi lực lượng và hoàn cảnh vượt ra 
ngoài phạm vi cá nhân. Do đó, Deutscher lý luận rằng, cách mạng sẽ thắng lợi vào 
năm 1917 với những nhà lãnh đạo khác ngay cả khi Lenin\index{Lenin} bị loại bỏ 
khỏi vũ đài chính trị bởi một tai nạn nào đó. Chính bản thân Trotsky\index{Trotsky}, 
hoặc một nhóm những người đứng đầu Bolsevik khác, sẽ lấp chỗ trống mà 
Lenin\index{Lenin} để lại.

Deutscher đoán rằng Trotsky\index{Trotsky} sa vào ranh giới chủ quan "sùng bái cá nhân 
" Lenin\index{Lenin} là bắt nguồn từ động cơ phải có một nhu cầu tâm lý để phóng đại vai 
trò lãnh đạo của cá nhân nhằm cân bằng lại chế độ chuyên quyền của Stalin\index{Stalin} trong cuộc 
đấu tranh chính trị đầy chết chóc với Stalin\index{Stalin}. Ông ta tìm cách sửa chữa Trotsky\index{Trotsky} 
bằng cách viện đến những tư tưởng trong bài viết kinh điển của Plekhanov\index{Plekhanov} 
về _Vai trò của cá nhân trong lịch sử_. Tác phẩm đó là bút chiến chống lại trường 
phái xã hội học chủ quan Narodnik\index{Narodnik} ca ngợi anh hùng là người tự 
mình sáng tạo lịch sử thay vì quần chúng và các nhân tố quyết định khách quan khác 
của cuộc đấu tranh giai cấp. Tranh luận chống lại định đề rằng đòi hỏi của quần chúng cần 
người lãnh đạo có thể được đáp ứng chỉ bởi duy nhất một cá nhân kiệt xuất, Plekhanov
chỉ ra rằng người đó khi đã leo lên vị trí quyền lực tối cao sẽ tìm cách ngăn cản 
những người khác mà họ cũng có thể thực hiện công việc ấy, nhưng theo cách thức khác.
Sự che khuất của những ứng viên có thể thay thế được tạo ra ảo tưởng về vai trò duy nhất
không thể thay thế của cá nhân. Nếu như những điều kiện tiên quyết đã chín muồi và 
đòi hỏi của lịch sử có đủ  sức mạnh, sẽ có một loạt những cá nhân có thể đảm nhận chức 
năng lãnh đạo.
  
Ví dụ ở Trung Quốc\index{Trung Quốc} và Nam Tư\index{Nam Tư}, Deutscher\index{Deutscher}
viết, minh chứng cho thấy cuộc cách mạng đang lên có thể sử dụng những con người 
có tầm vóc thấp hơn Lenin\index{Lenin} hay Trotsky\index{Trotsky} để chiếm lấy 
quyền lực. Đấu tranh giai cấp có thể tạo sức ép buộc mọi nguyên liệu con người 
đang sẵn có vào phục vụ cho việc hoàn thiện mục tiêu của nó.

Chủ đề này có tầm quan trọng hơn cả đánh giá của Trotsky\index{Trotsky} về vai trò 
của Lenin\index{Lenin} trong cuộc cách mạng Nga hoặc quan trọng hơn cả sự phê phán 
của Deutscher cho rằng Trotsky\index{Trotsky} không nhất quán về vấn đề trên. 
Tác động tương hỗ giữa các nhân tố chủ quan và khách quan trong tiến trình lịch sử 
là một trong những vẫn đề mấu chốt của khoa học xã hội. Nó cũng là chìa khóa cho 
thực tiễn cách mạng trong thời đại của chúng ta.

Chủ nghĩa duy vật lịch sử dứt khoát gán tính quyết định, Deutscher\index{Deutscher} 
nhấn mạnh, cho nhân tố khách quan như trình độ của lực lượng sản xuất và tình 
trạng của quan hệ giai cấp trong quá trình làm nên lịch sử. Nhưng còn nhiều vấn 
đề hơn thế.

Đầu tiên là, các hiện tượng xã hội được chia thành các phạm trù mà chúng chỉ có tính 
chủ thể hoặc khách thể một cách tương đối. Trạng thái của các phạm trù ấy có thể 
thay đổi theo mối liên kết thích hợp. Nếu như môi trường thế giới xung quanh là 
khách thể đối với quốc gia mà nó thuộc về môi trường ấy, quốc gia này đến lượt nó lại
là khách thể đối với các giai cấp hình thành nên cấu trúc xã hội của nó. Giai cấp 
thống trị là khách thể của giai cấp công nhân. Đảng là chủ thể của giai cấp mà nó 
đại diện lợi ích, trong khi đó các nhóm, xu hướng, đảng phái và các tổ hợp khác 
lại là chủ thể của phong trào hay đảng phái chứa đựng chúng. Cuối cùng, cá nhân là 
chủ thể trong mối quan hệ với tất cả nhân tố khác, mặc dù anh ta là một tồn tại 
khách thể trong quan hệ với những cá nhân khác.

Thứ hai là, các nhân tố đa dạng trong mọi tiến trình lịch sử không, và thực 
ra là không thể, phát triển đồng mức và đồng thời. Không chỉ ở chỗ có một 
số nhân tố chín muồi trước nhân tố khác, mà còn là một số nhân tố còn không thể đạt 
tới một kết quả hoàn thiện và đầy đủ tại những thời điểm quyết định, hoặc thực ra là 
tại mọi thời điểm. Sự xuất hiện cùng lúc của _tất cả_ các nhân tố khác nhau thiết 
yếu để xảy ra một kết cục nhất định trong tiến trình lịch sử lớn lao là một sự kiện 
vô cùng đặc biệt hoặc "tình cờ (ngẫu nhiên)" mà nó chỉ là tất yếu khi xét trong 
một quá trình lâu dài. 

Lãnh đạo, tập thể và cá nhân gộp thành các thành phần có ý thức trong lịch sử. 
Ảnh hưởng của một cá nhân trong việc quyết định tiến trình của các sự kiện có thể 
từ mức độ quyết định không đáng kể tới mức độ quyết định toàn bộ. Phạm vi tác động 
của anh ta phụ thuộc vào điều kiện lịch sử đã phát triển đến giai đoạn nào, 
tương quan giữa các lực lượng xã hội, và mối liên kết chính xác của anh ta với 
các lực lượng ấy tại một tình huống xác định.

Có những thời gian dài người làm cách mạng có ý chí mạnh mẽ nhất cũng bất lực 
trước làn sóng các sự kiện và trên thực tế không thể dẫn dắt được các sự kiện ấy. 
Mặt khác, khi những "đợt triều hành động của con người trở thành thác lũ, họ dẫn 
dắt nó đi tới kết cục quyết định". 

Thông thường, hành động của cá nhân diễn ra ở đâu đó giữa hai thái cực trên. Những 
gì cá nhân thực hiện - hay không thực hiện, nằm trong năng lực của cá nhân, có thể 
có tác động ở mức độ giới hạn nào đó đến tốc độ và đặc điểm nhất định của diễn biến 
chính [của lịch sử]. 

Vấn đề là ở chỗ ở đâu và khi nào một cá nhân vận dụng tối đa mức độ ảnh hưởng và 
trở thành nhân tố quyết định tới kết cục của cuộc đấu tranh? Điều này chỉ xảy ra khi 
cá nhân can thiệp vào đúng thời điểm cao trào của quá trình tiến hóa lâu dài, khi 
mà mọi nhân tố khách quan hơn đã trở thành hiện thực. Những nhân tố này sắp đặt 
sân khấu cho người đóng vai trò quyết định và trang bị phương tiện giúp tiến hành 
mục tiêu và cương lĩnh của phong trào mà anh ta đại diện. 

Cá nhân giúp khởi tạo ra con đường phát triển mới ở bất kỳ lĩnh vực nào trở thành 
mắt xích cuối cùng trong chuỗi các sự kiện. Chúng ta đều quen thuộc với thành ngữ 
quá mù hóa mưa hay giọt nước làm tràn ly. Cá nhân tạo ra sự khác biệt có vai trò 
như chất xúc tác giúp lượng biến đổi thành chất trong quá trình mà cái mới thay 
thế cái cũ.

Nhưng, cá nhân phải can thiệp vào đúng thời điểm bước ngoặt của tiến trình thì mới 
đem lại ảnh hưởng quyết định. Thời điểm thuận lợi đó, không phải lúc nào cũng lệ 
thuộc vào ý thức của bản thân cá nhân, cho phép anh ta trở thành nguyên nhân sau 
rốt trong chuỗi những điều kiện đã tích tụ, tức các nhân tố quyết định thiết yếu 
cho kết quả cuối cùng.

Sự khác biệt mà Deutscher\index{Deutscher} ghi nhận giữa quan điểm của Trotsky\index{Trotsky} 
rằng Lenin\index{Lenin} là không thể thiếu được cho thắng lợi của Cách Mạng 
Tháng Mười với quan điểm rằng những quy luật khách quan của lịch sử mạnh mẽ hơn 
nhiều so với tính cách đặc biệt của nhân vật chính tham gia là cần phải được giải 
thích bằng sự khác biệt giữa tiến tình ngắn hạn và tiến trình dài hạn của lịch sử. 
Những phép tính xác suất áp dụng cho cả lịch sử nhân loại cũng như áp dụng cho các 
sự kiện trong tự nhiên. Khi có đủ thời cơ sau khoảng thời gian dài, các lực lượng 
đại diện cho tất yếu khách quan của tiến bộ xã hội sẽ vượt qua mọi trở ngại và chứng 
tỏ nó mạnh mẽ hơn mọi sự phòng thủ của trật tự cũ. Nhưng không cần thiết đúng phải 
như vậy tại mọi giai đoạn hoặc trong mọi hoàn cảnh. Ở đây phẩm chất lãnh đạo có 
thể quyết định chọn trong những khả năng thực sự đã nảy sinh từ những điều kiện 
đang thắng thế để biến nó thành hiện thực.

Nhân tố ý thức có tầm quan trọng khác biệt về chất trên toàn bộ kỷ nguyên lịch sử
hơn hẳn tầm quan trọng mà nó có được trong một giai đoạn hay một pha nhất định 
nào đó. Khi những lực lượng xã hội đối kháng tranh giành vị trí thống trị trên quy mô 
lịch sử thế giới, những tình huống có lợi hay bất lợi như tính cách của lãnh đạo 
có xu thế triệt tiêu lẫn nhau. Tất yếu lịch sử đang diễn ra khẳng định bản thân 
nó ở bên trong và thông qua những cuộc đấu tranh và nó lấn át những đặc tính 
bề ngoài và ngẫu nhiên. [Những đặc tính ấy có thể quyết định kết quả cuối cùng 
của bất cứ một cơ hội cụ thể nào]. Hơn nữa, giai cấp đang lên trong cuộc đấu tranh lâu dài 
có lợi hơn đối thủ được phát sinh một cách ngẫu nhiên, bởi giai cấp đang suy 
thoái có ít sức mạnh và ít sức đề kháng để chống lại những biến động nhỏ trong 
tương quan lực lượng. Như tổng tài sản của ai đó tăng lên thì tài sản của những
người khác phải giảm đi. 

Thời cơ là nhân tố quan trọng nhất trong cuộc xung đột giữa các lực lượng xã hội
đang đấu tranh lẫn nhau. Giai đoạn không xác định, khi mà các sự kiện có thể bị đẩy theo 
hướng này hay hướng khác, không tồn tại lâu. Khủng hoảng trong quan hệ xã hội 
phải được giải quyết nhanh chóng bằng cách này hay cách khác. Tại thời điểm đó, 
tính chủ động hoặc tính bị động của những cá nhân, nhóm, đảng hay đám đông 
đang chiếm ưu thế, có thể làm lật cán cân từ phía này sang phía khác. Cá nhân có 
thể tham dự với tư cách là nhân tố cuối cùng trong toàn bộ quá trình 
quyết định lịch sử chỉ khi mọi lực lượng tham dự khác đang tạm thời cân bằng lẫn 
nhau. Chỉ cần thêm một sức nặng cũng có thể làm thay đổi sự cân bằng giữa các 
lực lượng. 

Hầu như ai cũng có thể nhớ lại những lúc mà can thiệp của bản thân hoặc của 
người khác quyết định đến giải quyết một tình huống bất định. Những gì xảy ra 
ở những biến cố nhỏ của cuộc đời áp dụng được cho cả những sự kiện lớn. Chỉ một 
phiếu bầu cũng có tính quyết định nếu như những lực lượng quyết định 
đến một vấn đề nào đó đang cân bằng nhau, do vậy phẩm chất xuất sắc của nhân vật
có tầm vóc sẽ được bộc lộ vào thời điểm lịch sử đang rơi vào bế tắc. Quyết định 
hoặc tính quyết đoán của họ giúp phá vỡ thế bí và đẩy sự kiện đi theo một lộ 
trình xác định. 

Điều này đúng cho cả xu thế cách mạng lẫn xu thế phản cách mạng. Hitler đóng 
vai trò quan trọng bởi vì Hitler dẫn dắt nước Đức đến chủ nghĩa phát-xít và đến 
chiến tranh. Nhưng Hitler không dẫn dắt nước Đức hay lịch sử thế giới đến một 
xu thế mới về chất. Ông ta chỉ đơn thuần viết lên một chương kinh hoàng tiếp 
theo trong cái chết đau đớn của chủ nghĩa tư bản. 

Đóng góp không thể thiếu của Lenin là giúp mở ra một con đường hoàn toàn mới cho
lịch sử Nga và cho lịch sử thế giới, dẫn dắt nó từ ngõ cụt của chủ nghĩa tư bản 
đến sự khởi đầu của chủ nghĩa xã hội. 

Chúng ta quay lại vấn đề cụ thể mà Deutscher\index{Deutscher} tranh luận. Ông ta 
không đặt vấn đề là, khi cách mạng 1917 thực tế diễn ra, Lenin\index{Lenin} đóng 
vai trò là nguyên nhân cuối cùng dẫn đến thắng lợi của Cách mạng Tháng Mười. 
Khác biệt giữa Deutcher\index{Deutscher} và Trotsky\index{Trotsky} liên quan đến 
sự không chắc chắn của các khả năng lịch sử. Liệu một nhà cách mạng khác như 
Trotsky\index{Trotsky} chẳng hạn, hoặc một tập hợp trong số họ, có thể thay thế 
được vị trí của Lenin\index{Lenin}?

Trotsky\index{Trotsky} trả lời dứt khoát là không thể. Deutscher\index{Deutscher} 
phản đối rằng nếu như những người khác không thể thực hiện được nhiệm vụ lãnh đạo đó, 
thế thì lập trường của chủ nghĩa duy vật lịch sử về tính quyết định có tính quy luật 
của các sự kiện phải bị bác bỏ. Hoặc nhân tố chủ quan hoặc nhân tố khách quan có 
vai trò quyết định; cần phải lựa chọn một trong số đó. 

Theo ý kiến cá nhân tôi, Deutscher\index{Deutscher} có lập trường quá hẹp hòi và 
phiến diện về lịch sử quyết định luận, trong khi Trotsky\index{Trotsky} áp dụng 
cách giải thích linh hoạt hơn và da diện hơn dựa trên sự tương quan và đối lập 
lẫn nhau của các phạm trù. Ông kiểm chứng nhận định của mình, trên hết là bằng 
thực tiễn, tiếp đến là bằng lý luận, trong suốt các giai đoạn liên tiếp của 
Cách mạng Nga ở đó các nhân tố có ý thức nổi lên một cách khá rõ ràng.

Hình thức lãnh đạo ở hai cuộc cách mạng trong năm 1917 là rất khác nhau. Cách 
mạng Tháng Hai không được lập kế hoạch và không được dẫn dắt từ bên trên. Tại 
chương "Ai lãnh đạo Cách mạng Tháng Hai?", trong tác phẩm _Lịch sử Cách mạng Nga_ 
của Trotsky\index{Trotsky}, ông đã chỉ ra rằng Cách mạng Tháng Hai được dẫn dắt bởi 
"những người công nhân có ý thức và đã tôi luyện được giáo dục chủ yếu bởi đảng 
của Lenin". Với vai trò là người giáo dục và người tổ chức cho những công nhân 
nòng cốt ấy, Lenin\index{Lenin} trong chừng mực đó là thiết yếu cho cuộc nổi 
dậy Tháng Hai, ngay cả khi ông không trực tiếp có mặt trên chiến tuyến. 

Giữa Cách mạng Tháng Hai và Cách mạng Tháng Mười, Lenin\index{Lenin} ngày càng trở nên 
quyết đoán bởi lập trường kiên định và tầm nhìn xa vào một loạt các thời khắc 
sống còn, bắt đầu bằng việc định hướng lại các thành viên Bolshevik vào Tháng Tư 
và đến cao trào là sự kiên định của Lenin vào cuộc nổi dậy Tháng Mười. Theo Trotsky\index{Trotsky}, 
vai trò của Lenin\index{Lenin} là không thể lặp lại. Không chỉ đơn thuần vì tài 
năng của Lenin\index{Lenin} mà còn là chỗ đứng đặc biệt của Lenin trong đảng 
Bolshevik, đảng mà phần lớn do Lenin\index{Lenin} sáng tạo ra. 

Vấn đề lãnh đạo trong Cách mạng Nga có tính hai mặt. Những người Bolshevik dẫn
dắt công nhân và nông dân đến thắng lợi, Lenin\index{Lenin} dẫn dắt đảng Bolshevik. 
Vai trò vĩ đại của người đến từ một thực tế là người đã _dẫn dắt những người lãnh đạo_ 
của cuộc cách mạng. 

Trotsky\index{Trotsky} hiểu rõ hơn ai hết Lenin có thể chiếm giữ nấc thang cao hơn 
cũng như đội ngũ trong đảng của Lenin\index{Lenin} cũng có thể làm vậy. Trong giai 
đoạn từ Tháng Tư đến Tháng Mười, uy tín của Lenin\index{Lenin} giúp sức đáng kể 
cho việc đạt được những nghị quyết đúng đắn vượt qua sự chống đối của những người 
đứng đầu Bolshevik khác. Sự tích tụ uy tín này không phải đạt được bằng cách loại 
bỏ những người khác, trong đó có cả Trotsky\index{Trotsky}, người có những mối 
quan hệ và có một lịch sử tổ chức khác biệt. Đó là nền tảng khách quan cho quan 
điểm của Trotsky\index{Trotsky} rằng Cách mạng Tháng Mười có thể không diễn ra 
trừ khi "Lenin\index{Lenin} có mặt và nắm quyền chỉ huy."

Chắc chắn là, như Deutscher\index{Deutscher} ghi nhận và như bản thân 
Trotsky\index{Trotsky} nhận ra, không thể rõ ràng trắng đen hoàn toàn ở quan 
điểm này. Nhưng kết luận của Trotsky, thể hiện trong mọi tác phẩm của ông sau 
Cách mạng Tháng Mười và trước khi Stalin nổi lên, không dựa trên sự sa ngã đáng 
tiếc vào chủ nghĩa chủ quan. Kết luận ấy là đến từ việc áp dụng phép biện chứng 
Marxist vào thực tiễn sự kiện mà ông chứng kiến và phân tích chúng. Nếu ông sai, 
thì đó không phải do sự xa rời nguyên tắc hoặc phương pháp nảy sinh từ động cơ 
chính-trị-tâm-lý vô ý thức nào, như Deutscher\index{Deutscher} vẫn xem là như vậy, 
mà là từ việc đánh giá sai các sự kiện.

Sidney Hook\index{Sidney Hook, Hook} tham gia vào cuộc tranh luận này từ một thái cực 
đối lập. Khi đánh giá về cuốn sách _Nhà tiên tri bị ruồng bỏ_ vào Tháng 11 năm 1964 
trên tờ _New Leader_ ông ta chộp lấy phê phán của Deutscher về chủ nghĩa chủ quan 
của Trotsky để phục vụ cho những mục đích của riêng mình. Thay vì lên án, ông ta
ca ngợi Trotsky\index{Trotsky} đã bỏ qua sự giáo điều trong chủ nghĩa duy vật 
biện chứng và quy kết "sự kiện xã hội quan trọng nhất trong lịch sử nhân loại" 
là do sự xuất hiện của Lenin\index{Lenin} ở nước Nga một cách tình cờ ngẫu nhiên 
và thuần túy cá nhân. Trong nhãn quan của ông ta, Cách mạng Tháng Mười là kết 
quả tình cờ của một cá nhân. Hook\index{Hook} lặp lại quan điểm này trong cuốn 
sách của mình _Người hùng trong lịch sử_, và được Deutscher\index{Deutscher} 
trích dẫn lại, rằng Cách mạng Tháng Mười "không phải là sản phẩm của toàn bộ quá 
khứ của lịch sử nước Nga mà là sản phẩm của một trong những nhân vật làm nên 
lịch sử vĩ đại nhất của mọi thời đại." 

Trong khi Deutscher nhân danh trường phái chính thống Marxist thiên về coi các
nhân tố khách quan là đầy đủ và do vậy coi nhẹ tầm quan trọng sống còn của vai 
trò lãnh đạo của Lenin\index{Lenin}, thì Hook\index{Hook} thực tế lại loại bỏ những 
yếu tố tiên quyết bằng cách coi chiến thắng của Cách mạng Tháng Mười lệ thuộc 
hoàn toàn vào một cá nhân đơn lẻ. Tiếp cận của ông ta không đạt tới tiêu chuẩn của 
những nhà sử học theo trường phái tự do xuất sắc nhất, những người mà ít ra cũng đặt 
các yếu tố khách quan ngang hàng với yếu tố tư tưởng và sự can thiệp của những cá nhân.

Hook\index{Hook} buộc phải xuyên tạc lập trường của Trotsky\index{Trotsky} nhằm biến 
Trotsky\index{Trotsky} thành một kẻ thực dụng hời hợt như chính bản thân Hook\index{Hook}.
Cuốn _Lịch sử_ của Trotsky tập trung một cách rõ ràng vào chứng minh 
_tính tất yếu của Cách Mạng Nga và thành quả cụ thể của nó_ là kết quả của toàn 
bộ quá trình tiến hóa của chủ nghĩa tư bản thế giới, của sự lạc hậu của nước Nga 
cùng với tư bản công nghiệp tập trung và giai cấp công nhân tiên tiến, sức ép của 
chiến tranh thế giới lần thứ nhất lên chế độ Nga hoàng thối nát, sự yếu kém của tư 
sản, sự thất bại của các đảng phái tiểu tư sản và tầm nhìn táo bạo của những người 
Bolshevik do Lenin dẫn đầu. 

Trotsky\index{Trotsky} chỉ ra sự vận hành của nhân tố quyết định ấy ở bên trong 
thực tại sống động bằng cách thuật lại và phân tích mối liên thông giữa các sự kiện
nổi bật từ đầu Tháng Hai cho đến cao trào Tháng Mười. Các giai đoạn kế tiếp của 
cuộc cách mạng không diễn ra một cách tùy tiện; các giai đoạn ấy được phát sinh 
một cách có tính quy luật không thể lay chuyển được từ giai đoạn khác tuân theo 
một chuỗi điều kiện nhân quả. Mục đích của việc giải thích về mặt lý thuyết của ông 
là để tìm ra trong các sự kiện của quá trình thực tiễn đã được kiểm chứng những 
kết quả của tất yếu khách quan được hình thành từ các quy luật của đấu tranh giai 
cấp khi áp dụng vào một cường quốc lạc hậu trong bối cảnh của thế kỷ 20. Ông 
đã lường trước và nói rõ những điều này trong học thuyết nổi tiếng của ông về 
cách mạng không ngừng. 

Trotsky\index{Trotsky} xem đảng Bolshevik là một trong những thành phần của tất 
yếu lịch sử, và Lenin\index{Lenin} là nhân tố nổi bật có ý thức nhất và là người thực 
hành nhuần nhuyễn nhất khoa học chính trị Marxist dựa trên những quy luật này. 
Không phải ngẫu nhiên mà Lenin\index{Lenin} có thể đảm nhận được vai trò mà 
ông đã thực hiện. Ông không phải là kẻ cơ hội. "Lenin\index{Lenin} không phải là 
yếu tố ngẫu nhiên trong tiến trình lịch sử, mà là sản phẩm của toàn bộ lịch sử phát 
triển của nước Nga." Hàng năm trời ông đã chuẩn bị cho bản thân và cho đảng của 
mình nhiệm vụ lèo lái cuộc cách mạng đang được kỳ vọng đến thắng lợi. 

Không có sự định trước nào nằm trong những điều kiện tiền đề cho Cách mạng 
Tháng Mười; những điều kiện ấy là lịch sử nước Nga, tầm nhìn và hiểu biết chính 
trị sâu sắc của Lenin\index{Lenin}. Thực tiễn chứng minh chúng gộp thành một tất 
yếu chung. Cũng không thể có một tiến trình các sự kiện thực tế nào có thể trở 
thành hiện thực mà thiếu sự có mặt đồng thời của nhiều tình huống ngẫu nhiên có 
lợi hoặc bất lợi cho cả hai phía. 

Chẳng hạn, có một may mắn là bộ tổng tham mưu Đức vì những lý do của riêng mình 
đã cho phép Lenin\index{Lenin} từ nơi lưu đày ở Thụy Sĩ qua Đức để trở về Nga kịp 
thời dẫn dắt đảng Bolshevik\index{Bolshevik}. Cũng là một ngẫu nhiên lịch sử khi 
Lenin vẫn sống sót và hoạt động sau nhiều tháng cam go; cũng có thể xảy ra thế khác, 
thực sự là, Lenin\index{Lenin} đã nghĩ mình sẽ bị giết. Trong trường hợp đó, nếu 
chúng ta tin vào Trotsky\index{Trotsky}, một kết cục có tính chất xã hội ẩn chứa
trong tình thế đó đã có thể không trở thành hiện thực vào năm 1917. 

Điều đó có nghĩa là lịch sử của thế kỷ 20, giờ đây không thể tưởng tượng nổi khi 
thiếu Cách mạng Nga cùng với mọi hậu quả, sẽ có thể rất khác -- không ở những 
đường nét lớn trong tiến trình của nó mà tất nhiên trong cả diễn biến cụ thể 
của cuộc đấu tranh giữa cách mạng xã hội với đối kháng tư bản của nó.

Không có gì gọi là phi Marxist, như Deutscher\index{Deutscher} dường như nghĩ vậy, 
khi thừa nhận điều này. Liên hệ "vận mệnh của nhân loại trong thế kỉ này" với hành
động của Lenin\index{Lenin} vào năm 1917 không phải là cách tư duy chủ quan; đó 
là sự thật. Trái lại, sự thiếu vắng Lenin\index{Lenin} có thể coi như là loại 
trừ những điều kiện thiết yếu cho thắng lợi khỏi biên của tính quyết định luận, 
điều đó có thể làm tiến trình lịch sử thế giới đi theo hướng hoàn toàn khác.

Vận mệnh vĩ đại của nhân dân Nga và của toàn bộ nhân loại là vào năm 1917 
_cả hai yếu tố tất yếu và ngẫu nhiên cùng xảy ra_ để đưa cuộc đấu tranh của công 
nhân và nông dân tới kết cục thích hợp. Điều đó không phải bao giờ cũng xảy ra 
trong hàng thập kỷ kể từ đó. 

Deutscher\index{Deutscher} làm suy yếu tranh luận của mình một cách đáng kể khi 
tập trung sự chú ý vào nước Nga. Vai trò của Lenin\index{Lenin} và đảng của ông 
trở nên rõ ràng dưới ánh sáng của những thất bại mà giai cấp công nhân ở Châu Âu 
và Châu Á trong những năm 20 và 30 phải gánh chịu bởi sự thiếu vắng tập thể và 
cá nhân lãnh đạo tầm cỡ như Bolshevik-Lenin. Thắng lợi của Cách mạng Tháng Mười 
cùng với những thất bại hậu Tháng Mười thuyết phục Trotsky\index{Trotsky}, 
người đã từng có lúc hoài nghi, về vai trò quyết định của bộ phận lãnh đạo trong 
tình huống cách mạng khách quan. Những kinh nghiệm này dẫn dắt ông tổng quát hóa 
thành nguyên tắc cơ bản để xây dựng cương lĩnh của Đệ Tứ Quốc tế\index{Đệ Tứ Quốc tế}, 
được chấp thuận vào năm 1938, đó là "khủng hoảng lịch sử của nhân loại là khủng 
hoảng của sự lãnh đạo." Đó là lý do vì sao ông dành những năm cuối cùng của cuộc 
đời vào nhiệm vụ thiết lập một tổ chức lãnh đạo dưới ngọn cờ của Đệ Tứ Quốc tế\index{Đệ Tứ Quốc tế}. 

Bất đồng của Deutscher\index{Deutscher} với Trotsky\index{Trotsky} về vai trò của 
Lenin\index{Lenin} trong Cách mạng Nga liên quan trực tiếp đến sự khác biệt giữa 
ông ta và Trotsky về vai trò của Trotsky sau thời kỳ Lenin. Deutscher xem khẳng 
định của Trotsky rằng sự thành lập Đệ Tứ Quốc tế là "công việc quan trọng nhất 
của đời tôi -- quan trọng hơn cả năm 1917, hơn cả giai đoạn nội chiến, hoặc mọi 
giai đoạn khác..." là một sai lầm. Ông ta tin rằng năng lượng dồn vào những nhóm 
Trotskyist phần lớn là lãng phí, bởi vì những điều kiện khách quan không thích 
hợp cho việc xây dựng một Quốc tế mới. Theo ông, Trotsky nên làm theo lời khuyên 
duy trì vai trò là người diễn giải các sự kiện thay vì cố gắng thay đổi tiến trình 
các sự kiện đó bằng những phương thức của một tổ chức cách mạng thế giới đối lập.

J.B. Stuart\index{J.B. Stuart} trả lời phê phán của Deutscher\index{Deutscher} 
về tính phi thực tế của Trotsky\index{Trotsky} trong Đệ Tứ Quốc tế\index{Đệ Tứ Quốc tế}
vào ngày 17 và 24 Tháng Tư năm 1964, trên các kỳ của _World Outlook_, và không 
cần thiết phải lặp lại tranh luận của ông ta tại đây. Ở đây chúng ta quan tâm 
chủ yếu đến lý do thực sự đằng sau lập trường của Trotsky. 

Deutscher\index{Deutscher} cho rằng Trotsky đánh giá sai tầm quan trọng của 
Lenin\index{Lenin} trong Cách mạng Nga, và vai trò của bản thân ông trong thời 
kỳ phản động ở quy mô thế giới sau khi Lenin chết, chỉ vì những lý do tâm lý mà 
đi ngược lại tính khách quan Marxist. Trotsky\index{Trotsky} thực tế đã đi đến 
lập trường của mình từ hai phía, dường như đối với chúng ta, là từ nhận thức của 
ông về yêu cầu của tiến trình cách mạng trong thời đại. Ông nghĩ rằng tất cả những 
điều kiện khách quan chủ đạo để lật đổ tư bản đã chín muồi. Những gì còn thiếu cho 
những Tháng Mười mới là sự có mặt của lãnh đạo có kiểu mẫu như Lenin\index{Lenin} 
và đảng Bolshevik vào năm 1917. Những cán bộ như vậy phải được tạo ra để ngăn chặn sự 
kém cỏi và sự quan liêu xảo trá đang đứng đầu các bộ phận khác nhau của phong 
trào công nhân làm phá hoại cơ hội cách mạng. Do vậy, chính trị thế giới, chứ 
không phải tâm lý cá nhân, là điều kiện tất yếu cho những kết luận của ông.

Sự thật là, như Deutscher\index{Deutscher} chỉ ra, quyền lực cách mạng được 
[chinh phục] ở Nam Tư\index{Nam Tư} và Trung Quốc\index{Trung Quốc} với những người 
lãnh đạo được đào tạo theo trường phái Stalin\index{Stalin}, trường phái mà không 
phù hợp với tiêu chuẩn Bolshevik của Lenin. Đại hội Tái Thống nhất của Đệ Tứ 
Quốc tế vào năm 1963 nhận thức sự phát triển này trong nghị quyết của mình, 
_Động lực của Cách mạng Thế giới Ngày nay_: "Sự yếu kém của kẻ thù ở những quốc 
gia lạc lậu đã mở ra những khả năng nắm quyền lực thậm chí với những công cụ trơ mòn." 

Thế nhưng, văn kiện cũng nhanh chóng bổ sung: "Sức mạnh của kẻ thù ở các nước đế
quốc đòi hỏi phải có một công cụ hoàn thiện hơn." Để nắm lấy quyền lực ở những 
thành trì của chủ nghĩa tư bản cũng như để quản trị quyền lực ở những nhà nước
của giai cấp công nhân đã bị suy đồi và méo mó, thì xây dựng mới các đảng cách 
mạng của quần chúng và đoàn kết họ vào một tổ chức quốc tế mới vẫn còn là một 
nhiệm vụ chiến lược trung tâm của giai đoạn hiện tại, nó không hề kém tính chiến 
lược như những ngày thời Lenin\index{Lenin} và Trotsky\index{Trotsky}.

Sự thống nhất biện chứng giữa nhân tố khách quan và chủ quan trong quá trình làm 
cách mạng được chứng minh và học thuyết hóa bởi Fidel Castro\index{Fidel Castro}
và những người thân cận. Nếu như một sự kiện lịch sử nào đó có thể được coi là 
tác phẩm của một con người, thì đó là Cách mạng Cuba\index{Cuba}. 
Castro\index{Fidel Castro} thực sự là một "lider maximo" [nhà lãnh đạo quan trọng
nhất].

Đáng chú ý là trong bài diễn văn về Marxism-Leninism vào 21 Tháng 12 năm 1961, 
Castro\index{Fidel Castro} giải thích rằng những người sáng lập Phong Trào 26 
Tháng Bảy đã không chờ đợi sự xuất hiện của _tất cả_ các điều kiện khách quan 
cần thiết cho thắng lợi cách mạng. Họ chủ định tạo ra những điều kiện cách mạng 
còn thiếu thông qua đấu tranh. Chiến tranh du kích của họ đã tạo ra sự thay đổi 
về tinh thần, tâm lý, chính trị cần thiết cho việc lật đổ chế độ 
Batista\index{Batista} tàn bạo.

Sự thay đổi cân bằng lực lượng theo hướng có lợi cho phe tiến bộ được xúc tiến
bởi một nhóm nhỏ những chiến sĩ cách mạng có ý thức ở Cuba minh chứng sống động
cho thấy nhân tố chủ quan có thể đóng vai trò quyết định đến mức độ nào trong quá 
trình làm nên lịch sử. Thế nhưng những dự định của Castro có thể thất bại và 
những chiến binh của ông có thể trở thành bất lực nếu như họ không nhận được
sự hưởng ứng, đầu tiên là từ nông dân ở vùng núi và tiếp đến là từ quần chúng ở 
vùng nông thôn và khu vực thành thị.

Những sự kiện diễn ra cách Cuba 90 dặm cũng cho thấy tính hai mặt của tầm quan 
trọng của cá nhân trong quá trình làm nên lịch sử. Vụ ám sát Kennedy\index{Kennedy} 
vào Tháng 11 năm 1963 không gây ra gián đoạn nghiêm trọng nào đến mọi hoạt động 
của chính phủ Mỹ cũng không làm chuyển dịch hướng đi của nó ở trong nước hay ở 
nước ngoài. Sau khi khôi phục quyền điều hành, Johnson\index{Johnson} theo đuổi 
về cơ bản vẫn những chính sách như người tiền nhiệm, tuy nhiên với thương hiệu 
Texas thay cho giọng điệu Harvard. Do vậy, sự biến mất của một cá nhân rất nổi 
tiếng và quyền lực cũng không mang lại hậu quả gì tới sự tự vận hành của giới 
cầm quyền tư bản. Những cá nhân ủng hộ tư bản xuất hiện rồi biến mất. Hệ thống 
vẫn còn đó. 

Đồng thời, người nắm giữ vị trí tối cao ở Hợp chủng Quốc Hoa Kỳ kiểm soát sức mạnh
quân sự khổng lồ lớn hơn mọi cá nhân trong lịch sử nhân loại. Vào ngày 4 Tháng 
Sáu năm 1964, Johnson huênh hoang về sức mạnh quốc gia "lớn hơn sức mạnh của 
tất cả các quốc gia trong lịch sử thế giới gộp lại." 

Tổng thống có thể phóng đi số tên lửa mang đầu đạn hạt nhân đủ để hủy diệt toàn 
bộ nhân loại. Ai có thể thắc mắc về tầm quan trọng lớn lao của cá nhân khi mà 
một quyết định của một con người có thể chấm dứt lịch sử nhân loại trên hành 
tinh này? Kennedy\index{Kennedy} phải đối mặt trực tiếp với khả năng này trong 
thời gian diễn ra khủng hoảng tại Caribe\index{Caribe} vào năm 1962.

Chắc chắn, người ngồi trong Nhà Trắng không hành động như một cá nhân đơn lẻ. 
Ông ta là người điều hành nước Mỹ, tổng tư lệnh các quân binh chủng, và có ý 
nghĩa hơn, là tay sai cho những kẻ tìm kiếm lợi nhuận, đó mới là những kẻ điều 
hành kinh tế và chính phủ. Vai trò của ông ta nói chung tuân theo những tất yếu 
khách quan của sự thống trị của độc quyền; và phân tích đến cùng, lợi ích căn bản 
của giai cấp thống trị quyết định hành vi chính trị của ông ta.

Nhưng chức năng mang tính đại diện của ông ta không thủ tiêu được một thực tế 
là một mình ông ta có thể được giao quyền để ra quyết định cuối cùng và ra lệnh
nhấn nút bom H.

Quyết định cá nhân này là thể hiện hoàn thiện của tính quyết định xã hội, 
mắt xích cuối cùng trong chuỗi nhân quả. Tính quyết định xã hội của thế giới 
ngày nay bị chia thành hai xu thế không thể thỏa hiệp, bắt nguồn từ hai giai 
cấp đối kháng nhau. Một xu thế được dẫn dắt bởi những kẻ gây chiến tư bản, người 
phát ngôn của nó là Hoa Kỳ đã tuyên bố rằng họ không kiềm chế việc sử dụng vũ 
khí nguyên tử nếu thấy cần thiết. Một xu thế khác được hình thành bởi quần chúng
nhân dân Mỹ và phần còn lại của thế giới khiếp sợ viễn cảnh này và sẽ mất tất
cả nếu điều đó xảy ra.

Nhân tố quyết định nào sẽ thắng thế? Định mệnh của nhân loại treo lơ lửng giữa
quyết định này. Để tước đoạt và giải trừ những kẻ điên rồ với vũ khi nguyên tử
trong tay ở trụ sở Washington này, phải xây dựng một phong trào cách mạng sâu rộng
và kiên định. Không một cá nhân đơn lẻ nào có thể ngăn chặn chúng lại. Nhưng thắng 
lợi của cuộc đấu tranh giữa cái sống và cái chết vì hòa bình thế giới chống lại 
sự hủy diệt hạt nhân đòi hỏi sự sáng tạo và tâm huyết của _những cá nhân_ cho dù 
họ không sở hữu những năng lực lãnh đạo xuất chúng như Lenin\index{Lenin}, 
Trotsky\index{Trotsky} hay Castro\index{Castro}, họ có thể hành động bằng lòng 
quả cảm.
 
