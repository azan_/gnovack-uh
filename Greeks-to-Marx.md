# Các học thuyết chủ đạo về lịch sử: từ Hy Lạp đến Marx {#cac-hoc-thuyet-chu-dao-ve-lich-su-tu-Hy-Lap-den-Marx}
\chaptermark{Các học thuyết chủ đạo về lịch sử}

Những người theo trường phái duy vật lịch sử xem như không trung thành với nguyên 
tắc của chính mình khi họ không giải thích lịch sử như là kết quả của một quá trình 
lâu dài, phức tạp và mâu thuẫn. Loài người đã làm nên lịch sử hàng triệu năm khi 
họ vượt qua tình trạng của loài linh trưởng để bước sang thời đại nguyên tử. 
Thế nhưng một khoa học về lịch sử, có khả năng làm sáng tỏ những quy luật chi phối
hành vi tập thể của chúng ta qua nhiều thời đại, là một thành tựu khá mới mẻ. 

Những nỗ lực ban đầu nhằm khảo sát hành trình lâu dài của lịch sử nhân loại, nghiên 
cứu nguyên nhân của nó, và thiết lập thành những giai đoạn theo những ranh giới 
một cách khoa học mới chỉ được thực hiện khoảng 20 thế kỷ trước. Nhiệm vụ này, 
cũng như nhiều nhiệm vụ thuộc các lĩnh vực khác, được khởi nguồn bởi người Hy Lạp.

Ý thức lịch sử là tiền đề cho khoa học lịch sử. Ý thức này không phải bẩm sinh
mà là một năng lực cần được nuôi dưỡng và phát sinh có tính lịch sử. Sự phân biệt 
các thời kỳ thành quá khứ, hiện tại và tương lai có gốc rễ từ sự tiến hóa trong 
tổ chức lao động. Nhận thức của con người về cuộc sống như chuỗi những sự kiện 
nối tiếp và biến động đã đạt tới chiều rộng và chiều sâu với sự phát triển và 
đa dạng hóa sản xuất xã hội. Lịch xuất hiện đầu tiên, không phải từ những nhóm 
hái lượm, mà từ những cộng đồng làm nông nghiệp.

Người nguyên thủy, từ thời hoang dã cho tới giai đoạn cuối cùng thời kỳ man rợ,
ít quan tâm đến quá khứ hay tương lai. Trải nghiệm và hành vi của họ làm nên
một phần lịch sử phổ quát khách quan. Nhưng họ vẫn chưa nhận thức được vị trí hay 
vai trò đặc biệt của mình trong sự phát triển của nhân loại. 

Họ chưa biết đến quan niệm về sự tiến triển của lịch sử từ giai đoạn này sang giai 
đoạn khác. Họ không cần tìm ra động lực thúc đẩy lịch sử cũng như họ không đánh 
dấu những giai đoạn phát triển xã hội. Ý thức tập thể của họ chưa đạt tới điểm để
có được một thế giới quan lịch sử hoặc sự thấu hiểu về xã hội học.

Năng lực sản xuất thấp, các hình thái kinh tế chưa chín muồi, lĩnh vực hoạt động 
còn hạn hẹp, sự nghèo nàn trong văn hóa và trong các mối quan hệ là bằng chứng 
giải thích tại sao họ có cái nhìn hạn chế trước các sự kiện diễn ra.

Lượng tri thức lịch sử mà người nguyên thủy sở hữu có thể đo đếm được từ những 
quan sát do thầy tu dòng Jesuit Jacob Baegert\index{Jacob Baegert} viết cách đây 
200 năm trong tác phẩm _Account of Inhabitants of the California Peninsula_. 
"Không có người California nào biết những sự kiện xảy ra từ trước lúc họ sinh ra, 
họ cũng không rõ bố mẹ mình là ai cứ như là họ đã mất bố mẹ từ khi còn ẵm ngửa...Người 
California tin rằng California là cả thế thới và ở đó họ là những cư dân duy nhất; 
bởi họ chẳng gặp ai khác, cũng không có ai khác đến gặp họ, mỗi nhóm nhỏ cứ ở trong 
ranh giới địa bàn nhỏ hẹp của mình." 

Trước khi người Tây Ban Nha tới, họ chỉ đánh dấu duy nhất một sự kiện có lặp lại,
thời điểm thu hoạch quả pitahaya. Do vậy, khoảng thời gian ba năm được gọi là 3 
pitahaya. "Họ hiếm khi dùng cụm từ đó, vì hiếm khi họ nói với nhau về năm, mà chỉ 
đơn thuần nói, 'lâu lắm rồi', hay 'chưa lâu lắm', thờ ơ hoàn toàn với thực tế là đã 
2 năm hay 20 năm kể từ khi sự kiện nào đó xảy ra."

Cho đến vài nghìn năm trước, con người mới cho rằng tổ chức quan hệ xã hội của riêng 
họ là tồn tại hiển nhiên. Đối với họ tổ chức ấy xuất hiện một cách cố định như trời đất 
và một cách tự nhiên như tai mắt của họ. Những người cổ đại không phân biệt bản thân 
họ với phần còn lại của tự nhiên, hay vạch ra ranh giới rõ rệt phân tách họ với 
nhưng sinh vật họ cùng sinh sống. Cần một thời gian rất dài để họ học cách phân 
biệt những gì thuộc về tự nhiên với những gì thuộc về xã hội. 

Chừng nào quan hệ xã hội còn đơn giản và ổn định, sự thay đổi diễn ra một cách 
chậm chạp đến mức hầu như không cảm nhận được trên khoảng thời gian dài, xã hội sẽ 
tan biến vào phía nền của thế giới tự nhiên. Trải nghiệm của thế hệ này không khác 
biệt gì lắm với trải nghiệm của thế hệ khác. Nếu một tổ chức quen thuộc với những 
thủ tục truyền thống bị đứt gãy, nó hoặc sẽ biến mất hoặc sẽ được tái lập trên 
những khuôn mẫu cũ. Hơn nữa những cộng đồng bao quanh nó, trong phạm vi họ được 
biết tới (và việc gặp gỡ nhau cũng không kéo dài theo cả không gian và thời gian), 
vẫn không có gì thay đổi. Trước khi người Châu Âu đặt chân tới, người Anh-điêng ở 
Bắc Mỹ có thể di chuyển từ Đại Tây Dương đến Thái Bình Dương, hay người thổ dân Úc 
có thể đi hàng ngàn dặm, mà không gặp phải một tổ chức xã hội nào có khác biệt một 
cách căn bản.

Trong hoàn cảnh ấy, nhìn chung không có xã hội nào hoặc không có hình thức sinh 
sống của một cá nhân nào được xem như một đối tượng đặc biệt đáng quan tâm hay 
đáng nghiên cứu. Nhu cầu phải đưa ra học thuyết về lịch sử hoặc về bản chất xã hội 
chỉ nảy sinh khi nền văn minh phát triển, đột nhiên những biến động mạnh mẽ có tác 
động sâu rộng vào quan hệ xã hội nảy sinh trong cuộc đời của những cá nhân hoặc 
trong ký ức của những thế hệ trước. 

Khi những bước chuyển biến mau lẹ diễn ra từ một hình thức cấu trúc xã hội này 
sang một hình thức khác, phương cách cũ đứng tách ra tương phản, thậm chí xung 
đột với phương cách mới. Bằng giao thương, đi lại và chiến tranh những đại diện 
cho sự bành trướng của hệ thống xã hội đang được thiết lập hoặc đang được 
tái thiết lập, tiếp xúc với những người có phong tục hoàn toàn khác biệt đang 
trong tình trạng phát triển văn hóa ở cấp độ thấp hơn. 

Sự khác biệt rõ rệt và trực tiếp hơn trong điều kiện sinh sống trong những cộng 
đồng rồi những xung đột gay gắt giữa những giai cấp đối kháng khiến những con người 
tư duy, những người có phương tiện để mà theo đuổi việc chiêm nghiệm về nguồn gốc của
những xung đột ấy, họ so sánh những hình thức xã hội và chính quyền khác nhau, và cố
gắng sắp xếp chúng thành những trật tự theo giá trị hay theo sự nối tiếp. 

Nhà sử học người Anh M.I. Finley\index{M.I. Finley} đưa ra quan điểm tương tự 
trong ba cuốn sách mới đây về Phương đông cổ đại vào ngày 20 Tháng 8 năm 1965, 
_New Statesman_: "Sự xuất hiện hay thiếu vắng 'ý thức lịch sử' không gì khác là 
sự phản chiếu tri thức về sự khác biệt của bản thân tiến trình lịch sử."

Ông ta trích dẫn học giả Marxist, Giáo sư D.D. Kosambi\index{D.D. Kosambi}, người 
quy kết "sự thiếu vắng hoàn toàn ý thức về lịch sử" ở người Ấn Độ cổ đại vào thế giới 
quan nhỏ hẹp của cuộc sống làng xã giới hạn bởi hình thức sản xuất nông nghiệp. 
"Mùa tiếp theo là cực kỳ quan trọng, trong khi đó chỉ có những thay đổi tích tụ 
ít ỏi được ghi chép lại từ năm này sang năm khác. Điều đó đem lại cảm giác chung 
cho những người quan sát bên ngoài về một 'Phương Đông bất tận, nơi thời gian 
như dừng lại'."

Những người ở nền văn minh khác thuộc Trung và Cận Đông cổ đại cũng thiếu ý thức 
về lịch sử. Giáo sư Leo Oppenheim viết, "Ở nền văn minh Lưỡng Hà không có bằng 
chứng nào trong tư liệu ghi ghép thể hiện một nhận thức về tồn tại của sự nối 
tiếp lịch sử ." Điều này được khẳng định bởi thực tế là "những bản khắc dài nhất và 
rõ ràng nhất của hoàng gia Assyrian ... được kẹp trong những phần cấu trúc của 
đền thờ và cung điện, an toàn khỏi sự nhòm ngó của con người và chỉ để đọc bởi 
những vị thần mà nội dung bản khắc nói đến.

Tiền đề cho thế giới quan lịch sử Phương Tây được ra đời từ năm 1100 đến năm 700 
TCN vào lúc chuyển tiếp từ Thời kỳ Đồ đồng sang Thời kỳ Đồ sắt ở những nền văn minh 
ở Trung Đông và Aegea. Những vương quốc và vùng dân cư làm nông nghiệp tương đối 
tự túc đã bị bổ sung và thay thế bằng những trung tâm thương mại bận rộn, đặc biệt
là ở các bến cảng của người Phoenic và người Ion thuộc vùng Tiểu Á. Ở đó, xuất hiện
những tầng lớp mới, -- thương nhân, chủ tàu thuyền, người sản xuất, thợ thủ công, 
người đi biển, -- họ đến và đi và thách thức những thiết chế, những lý tưởng và 
quyền lực của địa chủ cũ. Chế độ nô lệ gia trưởng bị thay thế bởi chế độ chiếm 
hữu nô lệ. Quan hệ hàng hóa, tiền kim loại, vay nợ bằng cầm cố tài sản làm xói 
mòn những cấu trúc xã hội cổ xưa. Những cuộc cách mạng dân chủ đầu tiên và những 
cuộc phản cách mạng của giới cai trị được ấp ủ trong lòng những thành quốc. 

Người Hy Lạp ở Ion, là những người đầu tiên quyết định viết lịch sử thành văn, 
họ là những người có liên hệ với thương nhân, là kỹ sư, là thợ thủ công và là 
những nhà thám hiểm. Hecaeteus\index{Hecaeteus}, người tiên phong trong số những 
nhà sử học Phương Tây, cũng sống ở thành phố thương mại thuộc vùng Miletus, ông 
cũng là một trong những nhà khoa học và triết học đầu tiên; ông trung thành với 
xu hướng tư tưởng duy vật.

Viết sử nhanh chóng làm nảy sinh những quan tâm về khoa học lịch sử. Một khi thói 
quen nhìn nhận các sự kiện thành xâu chuỗi được thiết lập, vấn đề nảy sinh là: 
Lịch sử diễn ra thế nào? Liệu có thể nhận ra được hình mẫu nào đó trong quá trình 
luôn biến động ấy? Nếu có thì nó là gì? Và nguyên nhân của nó là gì?

Những giải thích có lý trí về quá trình lịch sử được thực hiện bởi những sử gia 
xuất chúng người Hy Lạp từ Herodotus\index{Herodotus} tới Polybius\index{Polybius}. 
Đó là quan điểm vòng lặp (chu kỳ) trong sự vận động của lịch sử. Theo quan điểm này, 
xã hội, giống như tự nhiên, trải qua những hình thái phát triển giống hệt nhau 
theo những chu kỳ lặp đi lặp lại.

Thucydides\index{Thucydides}, nhà sử học nổi tiếng người Hy Lạp, tuyên bố rằng ông ghi chép về chiến 
tranh Peloponnesian nhằm răn dạy những bài học lịch sử bởi những sự kiện tương tự 
chắc chắn sẽ lại diễn ra. Plato\index{Plato} đưa ra học thuyết về Năm Vĩ đại\index{Năm Vĩ đại} 
(the Great Year), vào cuối năm ấy các hành tinh sẽ lại chiếm những vị trí cũ và 
mọi sự kiện dưới trần gian sẽ lặp lại. Quan niệm này được diễn đạt thành một câu 
châm ngôn trong _Ecclesiates_\index{Ecclesiates}: "Dưới ánh dương chẳng có gì là mới mẻ."

Quan niệm rằng hoạt động của con người có tính chất chu kỳ bắt nguồn từ quan điểm 
rằng Định mệnh là không thể thay đổi, bí hiểm, là toàn năng và sẽ thay thế các vị 
thần chi phối toàn bộ lịch sử. Định mệnh được thần bí hóa thành Ba Vị Thần Mệnh 
và xa hơn nữa được lý trí hóa bởi những con người có tri thức thành quy luật 
tối thượng về cuộc sống. Quan niệm về một vận mệnh bi kịch [vũ trụ] mà con người 
không thể kháng cự hay thoát khỏi trở thành một chủ đề chính trong kịch kinh điển 
Hy Lạp và cũng như trong tác phẩm lịch sử của Herodotus.

Những so sánh giữa các dân tộc khác nhau, hay giữa những quốc gia ở giai đoạn 
phát triển xã hội, kinh tế, chính trị khác nhau, sản sinh ra, cùng với ý niệm 
đầu tiên về sự tiến bộ lịch sử, một khoa học lịch sử so sánh. Ngay từ thế kỷ thứ 
8 TCN, nhà thơ Hesiod\index{Hesiod} đã đề cập thời kỳ đồ đồng diễn ra trước thời 
kỳ đồ sắt. Vài thế kỷ sau, Herodotus\index{Herodotus}, nhà nhân loại học đầu tiên, 
cha đẻ của môn lịch sử, thu thập các thông tin có giá trị về phong tục của người 
dân vùng Địa Trung Hải sống trong các thời kỳ nô lệ, trong thời kỳ man rợ và 
trong thời kỳ văn minh. Thucydides chỉ ra rằng người Hy Lạp cũng từng sống giống 
như những người man rợ đang sống trong thời đại của ông. Plato\index{Plato}, 
trong tác phẩm _Cộng hòa_, _Luật_ và các tác phẩm khác, rồi Aristotle\index{Aristotle}, 
trong tác phẩm _Chính trị_, đã thu thập những hình mẫu khác nhau về nhà nước cai trị. 
Họ định danh, phân loại và phê phán những hình mẫu ấy. Họ tìm tòi để xác định 
không chỉ hình thức tối ưu cho chính phủ thành quốc, mà còn về trình tự của sự 
phát triển các hình thức ấy và nguyên nhân của sự đa dạng về chính trị và về cách mạng. 

Polybius\index{Polybius}, nhà sử học người Hy Lạp nghiên cứu về sự trỗi dậy của 
đế chế La Mã, xem đó là ví dụ mẫu mực của quy luật tự nhiên chi phối sự biến đổi 
có tính chu kỳ từ hình thức chính phủ này sang hình thức chính phủ khác. Ông tin, 
như Plato tin, rằng mọi nhà nước đều không tránh khỏi việc trải qua những giai 
đoạn như chế độ vương quyền, quý tộc rồi dân chủ, những chế độ ấy thoái hóa thành 
những hình thức cùng loại gồm chuyên quyền, đầu sỏ, và quyền lực của đám đông. 
Sự nảy sinh và suy vong của những giai đoạn kế tiếp của hình thức cai trị là do 
những nguyên nhân tự nhiên. Ông viết, "Đó là chu kỳ thông thường của cách mạng 
hiến pháp, và là trật tự tự nhiên mà các tổ chức chính trị thay đổi, chuyển hóa 
và trở về giai đoạn ban đầu".

Ngay khi họ hiểu và đặt tên những hình thức chủ đạo của các tổ chức chính trị từ 
quân chủ đến dân chủ, các nhà tư duy người Hy Lạp thuộc cả trường phái duy tâm 
lẫn duy vật đã khai sinh các phương pháp cơ bản nhằm diễn giải lịch sử mà cho 
đến này vẫn còn tồn tại. 

Họ là những người đầu tiên cố gắng giải thích sự tiến hóa của xã hội theo quan điểm 
duy vật, cho dù nỗ lực đầu tiên ấy có thô ráp và vụng về thế nào. Những nhà theo 
trường phái Nguyên tử luận, những nhà Ngụy biện hay những nhà theo trường phái
y học Hippocrat\index{Hippocrat} đã đề xuất quan điểm rằng môi trường tự nhiên 
là nhân tố quyết định định hình lịch sử nhân loại. Biểu hiện cực đoan của quan 
điểm này, là xu hướng tư tưởng quy kết những biến động lịch sử-xã hội thành những 
hệ quả từ khu vực địa lý và điều kiện khí hậu. Polybius\index{Polybius} viết: 
"Chúng ta những kẻ phù du có xu hướng không thể cưỡng lại được là phải chịu sự 
tác động của khí hậu; và vì nguyên nhân này, chứ không phải nguyên nhân nào khác, 
làm nên sự khác biệt rành rành giữa chúng ta về tính cách, cơ thể và màu da, 
cũng như trong hầu hết thói quen của chúng ta, chúng cũng da dạng cùng với sự 
chia cắt về quốc gia và địa lý rộng lớn."

Những nhà xã hội học đầu tiên này chỉ ra rằng nhân loại phát triển từ thời kỳ 
man rợ đến thời kỳ văn minh bằng cách bắt chước tự nhiên và cải biến quá trình 
hoạt động của tự nhiên. Nhân vật tiêu biểu nhất của quan điểm duy vật này trong 
văn hóa Hy Lạp--La Mã là Lucretius người đã phác họa một cách xuất sắc về sự phát 
triển của xã hội trong bài thơ _Về bản chất của vạn vật_ (_On the Nature of Things_) 

Thế nhưng các học thuyết duy tâm về lịch sử lại thống trị trong các nhà tư tưởng 
Hy Lạp.

1. _Thuyết Thượng Đế Vĩ đại._ Nỗ lực sơ khai đầu tiên nhằm giải thích nguồn gốc 
và sự phát triển của thế giới và nhân loại là những câu truyện thần thoại của 
con người trước khi có chữ viết. Chúng ta quen thuộc với câu truyện thần thoại
trong _Sáng Thế_ (_Genesis_) cho rằng Chúa tạo ra trời đất và ngài làm việc theo
lịch có sáu ngày. Những câu chuyện hoang đường như vậy không có bất cứ tính xác 
thực khoa học nào.

    Người ta lần đầu tiên thu thập được những tư liệu thô về lịch-sử-thành-văn 
đích thực từ biên niên sử về các vị vua của những nền văn minh lưu vực sông ở 
Cận Đông, Ấn Độ và Trung Hoa. Quan điểm tổng hợp lịch sử đầu tiên nảy sinh từ 
sự kết hợp các yếu tố lấy ra từ câu truyện thần thoại cổ xưa với đánh giá từ những
tư liệu này. Đây là phiên bản lịch sử Thượng đế Vĩ đại, khẳng định rằng những tồn
tại thần thánh dẫn dắt sự vận động của con người và của vũ trụ.

    Ngay khi những bạo chúa thống trị các thành quốc và đế chế của họ, 
thì ý chí, sự đam mê, kế hoạch và nhu cầu của thượng đế trở thành nguyên nhân 
tận cùng để giải thích những sự kiện. Nhà vua là tác nhân duy trì thế giới đang 
tồn tại bằng cách hằng năm chiến đấu với những thế lực của sự hỗn loạn. Thuyết 
thần bí này được phát triển bởi người Summer, người Babylon và người Ai Cập trước 
khi chúng được người Hy Lạp và người La Mã tiếp nhận. Học thuyết ấy được trình 
bày chi tiết trong Kinh thánh của người Israel tiếp đến nó được tiếp thu và được
nhào nặn bởi các tôn giáo như Cơ đốc và Mohammed và bởi các quốc gia của họ.

    Dưới sự cai trị của chế độ quân chủ thần quyền, hoạt động của con người chịu sự 
dẫn dắt thần thánh của vị vua-kiêm-thầy-tế khoác lên vẻ bên ngoài như thần thánh.
Ở Babylon, Ai Cập, Đế chế Alexander và Rome, thế lực cai trị tối cao của vũ trụ và 
kẻ cai trị đầy quyền lực của vương quốc được coi là thần thánh như nhau.

2. _Thuyết Vĩ Nhân._ Thế giới quan thần luận đơn giản về lịch sử được coi là quá 
thô sơ và ngây thơ, quá gần gũi với thuyết vạn vật hữu linh nguyên thủy, quá 
xung đột với những khai sáng văn minh để có thể tồn tại mà không chịu sự phê phán 
hay sự thay đổi trừ khi nó được những kẻ dốt nát và cuồng tín ủng hộ. Nó bị thay 
thế bởi những phiên bản tinh tế hơn.

    Thuyết Vĩ Nhân nảy sinh từ sự phân ly của hai thế lực đối đầu trong Thuyết Thượng 
đế Vĩ đại. Sức mạnh vô song của các vị thần tập trung vào kẻ đứng đầu nhà nước, nhà
thờ, các tổ chức hoặc phong trào quan trọng khác. Nhân vật có địa vị cực kỳ quan 
trọng này xem như được ban phát khả năng nhào nặn các sự kiện một cách tùy ý. Đó 
là nguồn gốc cho niềm tin chắc chắn rằng những cá nhân với năng lực và sự ảnh 
hưởng khác thường sẽ quyết định hướng đi của lịch sử.
  
    Tôn thờ sùng bái Vĩ Nhân vẫn được tiếp nối từ thời kỳ những vị vua-thần ở 
Lưỡng Hà cho tới tôn sùng Hitler\index{Hitler}. Nó có nhiều dạng thức khác nhau tuân theo 
những giá trị của thời đại, của con người tùy thuộc từng lĩnh vực hoạt động xã hội. 
Trong thời kỳ cổ đại dạng thức ấy biểu hiện từ quân chủ thần thánh, bạo chúa, 
kẻ đặt ra luật lệ (Solon), kẻ chinh phạt quân sự (Alexander\index{Alexander}), kẻ độc tài 
(Caesar\index{Caesar}), anh hùng-người giải phóng (David\index{David}), đến các lãnh tụ tôn 
giáo (Jesu\index{Jesu}, Đức Phật\index{Đức Phật}, Nhà Tiên tri Mohammed\index{Mohammed}). 
Tất cả những người đàn ông này -- phụ nữ không có cơ hội để nổi trội trong xã hội 
phụ hệ -- được đặt vào vị trí Toàn Năng xem như là động lực chủ đạo của lịch sử.

    Nhân vật tiêu biểu được ca ngợi nhất hiện nay cho quan điểm này là 
Thomas Carlyle\index{Thomas Carlyle} ông ta viết: "Lịch sử phổ quát, lịch sử mà 
con người đã làm nên ở thế giới này, phân tích đến cùng là Lịch sử của Vĩ Nhân." 

3. _Thuyết Tư tưởng Vĩ đại._ Một biến thể triết học phức tạp hơn bám theo 
quan điểm Thần-Người Vĩ Đại là quan niệm rằng lịch sử được dẫn dắt hoặc được thúc 
đẩy bởi động lực tưởng tượng nhằm hiện thực hóa kết cục đã được sắp đặt từ trước. 
Nhà triết học Hy Lạp Anaxagoras\index{Anaxagoras} nói: "Lý trí (Nous) chi phối thế giới". 
Aristotle\index{Aristotle} kiên định với động lực đầu tiên [prime mover] của vũ trụ, và 
do vậy là cái vận động tột cùng của vạn vật, đó là Thượng Đế, được xem là lý trí 
thuần khiết tự tư duy cho chính bản thân nó.

    Hegel\index{Hegel} nhân vật tiêu biểu nhất của thời kỳ hiện đại của học thuyết 
cho rằng tiến bộ của nhân loại là ý tưởng hiện thực hóa và tuyệt đối. Ông viết: 
"Tinh thần, hay Tư duy, là nguyên lý vận động duy nhất của lịch sử." Mục đích 
của Tinh thần Thế giới, kết quả của sự phát triển đầy nhọc nhằn của nó, là sự hiện 
thực hóa tư tưởng tự do."

    Học thuyết Tư tưởng Vĩ đại dễ dàng rơi vào quan niệm cho rằng những tri thức kiệt
xuất, hoặc thậm chí một thiên tài nào đó, tạo ra động lực chính cho tiến bộ nhân
loại. Plato\index{Plato} chủ trương "ai đó phải là người nghiên cứu triết học và 
trở thành người lãnh đạo nhà nước; và những người khác sinh ra không phải để 
nghiên cứu triết học, thì là người làm theo thay vì làm lãnh đạo."
   
    Do đó, với niềm tin "Tư tưởng chi phối nhân loại", những nhà khai sáng của 
thế kỷ 18 tìm kiếm một vương quốc ánh sáng đề xuất xây dựng lại một nhà nước và 
xã hội tiến bộ. Một thể hiện phổ biến hơn của cách tiếp cận trên là đối lập với 
đám đông không xuy xét là một tầng lớp được coi như hình mẫu của lý trí mà chỉ 
riêng họ là có thể tin được để nắm vai trò lãnh đạo chính trị và nắm quyền lực.

4. _Thuyết Người Xuất chúng._ Tất cả những giải thích trên đều chứa đựng 
định kiến rằng một đẳng cấp nào đó, một Chủng tộc Xuất chúng, một quốc gia được 
lựa chọn, một giai cấp thống trị nào đó một mình nó làm nên lịch sử. Kinh Cựu ước 
cho rằng dân tộc Israel là những người được Thượng đế lựa chọn. Người Hy Lạp coi 
bản thân họ là tột đỉnh của văn hóa, ưu việt hơn ở mọi khía cạnh so với những dân 
tộc man rợ. Plato\index{Plato} và Aristotle\index{Aristotle} kính trọng tầng 
lớp quý tộc chủ nô như là những người ưu tú hơn mọi trật tự bên dưới.
 
5. _Thuyết Bản chất Con người._ Quan điểm lâu bền nhất cho rằng lịch sử được quyết 
định bởi bản chất con người, thiện hay ác. Bản chất con người, như bản thân tự 
nhiên, được xem là cứng ngắc, không thay đổi từ thế hệ này sang thế hệ khác. Nhiệm
vụ của các nhà sử học là tìm ra xem bản chất không thay đổi trong cá tính của 
con người là gì, tiến trình lịch sử chứng minh bản chất ấy như thế nào, và làm thế 
nào mà cấu trúc xã hội được thiết lập hoặc tái thiết lập theo bản chất ấy. Định 
nghĩa như vậy về bản chất con người là điểm xuất phát cho học thuyết xã hội của 
Socrates\index{Socrates}, Plato\index{Plato} và Aristotle\index{Aristotle} và 
những nhà duy tâm vĩ đại khác.
  
    Nhưng ta cũng sẽ tìm thấy thuyết bản chất con người trong những trường phái 
đa dạng nhất về triết học chính trị và triết học xã hội. Thật vậy, 
David Hume\index{David Hume} người theo chủ nghĩa kinh nghiệm khẳng định thẳng 
thừng trong _An Enquiry Concerning Human Understanding_: "Loài người vẫn không 
thay đổi, theo cả thời gian lẫn không gian, lịch sử cho chúng ta thấy đặc điểm này 
không có gì là mới mẻ hay kỳ lạ. Tác dụng chính của lịch sử là khám phá ra những 
nguyên tắc phổ quát và bất biến về bản chất con người."

    Nhiều nhà mở đường cho các ngành khoa học xã hội ở thế kỷ 19 đã bám vào 
"những nguyên tắc phổ quát và bất biến về bản chất con người." Chẳng hạn E.B. Tylor\index{E.B. Tylor}, 
người sáng lập ngành nhân chủng học ở Anh, viết năm 1889: "Giống như những khối 
đá phân tầng gối lên nhau thành một chuỗi về căn bản là đồng nhất trên toàn 
Địa cầu, những thể chế của nhân loại độc lập với những gì dường như chỉ là khác 
biệt bề ngoài về chủng tộc và ngôn ngữ, nhưng đều được định hình bởi cùng bản 
chất con người."

   Mặc dù họ có thể có những ý kiến khác nhau khi xác định những phẩm chất cơ bản 
của nhân loại là gì, những người duy tâm cũng như duy vật đều ủng hộ nguyên lý 
bản chất con người là không thay đổi để giải thích các hiện tượng lịch sử và xã hội. 
Do vậy, như M.I. Finley\index{M.I. Finley} nói với chúng ta trong lời tựa của 
cuốn sách "Những sử gia Hy Lạp" (_The Greek Historians_), Thucydides\index{Thucydides} 
sử gia có tư tưởng duy vật tin rằng "Bản chất và hành vi của con người là ... 
những phẩm chất cố định, không thay đổi từ thế kỉ này sang thế kỉ khác." 

Nhiều thế kỷ sau thời kỳ Hy Lạp cổ đại, hiểu biết khoa học về sự vận động của lịch
sử đạt được những tiến bộ ít ỏi. Dưới sự thống trị của Kitô giáo và chế độ phong kiến, 
quan niệm thần học cho rằng lịch sử là sự thể hiện kế hoạch của Thượng đế nhằm độc 
quyền thống trị triết học xã hội. Trái ngược với sự trì trệ trong khoa học ở Tây Âu, 
người Hồi Giáo và người Do Thái thúc đẩy tiến bộ trong khoa học tự nhiên cũng như 
trong khoa học xã hội. Người học trò sáng suốt nhất của xã hội giữa thời kỳ cổ đại 
và thời kỳ hiện đại là nhà triết học thuộc Thế kỷ 14 ở Maghreb, Ibn Khaldun\index{Ibn Khaldun}, 
ông đã phân tích sự phát triển của văn hóa Mohammed\index{Mohammed} và nguồn gốc của các 
thể chế bằng quan điểm duy vật triệt để nhất trong thời đại ông. Chính trị gia người 
Hồi giáo xuất sắc này dường như là học giả đầu tiên đưa ra quan niệm rõ ràng về xã 
hội học, tức khoa học về sự phát triển của xã hội. Ông thực hiện công việc ấy nhân 
danh nghiên cứu văn hóa.

Ông viết: "Lịch sử là ghi chép về xã hội loài người, hoặc về nền văn minh thế 
giới; về những biến động diễn ra trong bản chất xã hội đó, như sự tàn bạo, tính 
chất xã hội hóa, và sự đoàn kết tập thể; về cách mạng và sự nổi dậy của một 
tập hợp người này chống lại một tập hợp người khác với kết quả là hình thành 
vương quốc và nhà nước, với nhiều cấp độ khác nhau; về hoạt động hoặc nghề nghiệp 
khác nhau của con người, hoặc nhằm duy trì sự sống hoặc để làm khoa học hoặc 
để sáng tạo; và nhìn chung, mọi sự biến đổi mà xã hội trải qua là bởi chính bản 
chất con người." 

Bước tiến lớn tiếp theo của khoa học về lịch sử diễn ra cùng với sự trỗi dậy của xã 
hội tư sản và sự khám phá ra các vùng đất khác trên thế giới cùng với nó là sự 
bành trướng về giao thương và hàng hải. Xung đột với hệ thống phong kiến và 
Nhà thờ, những tri thức của lực lượng tư sản tiến bộ khám phá lại và khẳng định 
lại những tư tưởng về đấu tranh giai cấp mà đã được những người Hy Lạp đề cập 
đầu tiên, và họ hệ thống hóa những so sánh lịch sử với thời cổ đại nhằm củng 
cố tư tưởng của họ. Quan điểm cách mạng mới mẻ của họ không chỉ đòi hỏi một thế
giới quan rộng mở hơn mà còn đòi hỏi sự tìm tòi sâu hơn về phương thức thay đổi 
xã hội. 

Những đại diện nổi bật của tư tưởng tư sản là Machiavelli\index{Machiavelli} và Vico\index{Vico} 
ở Ý; Hobbes\index{Hobbes}, Harrington\index{Harrington}, Locke\index{Locke} và các nhà kinh tế học 
cổ điển Anh; Adam Ferguson\index{Adam Ferguson} ở Scotland; rồi Voltair\index{Voltair}, 
Rouseau\index{Rouseau}, Montesquieu\index{Montesquieu}, D'Holbach\index{D'Holbach} 
và những người khác ở Pháp họ đã sửa soạn một bức tranh thực tế hơn về xã hội và 
những tri thức phức tạp hơn về hình thái và các giai đoạn phát triển của xã hội.

Ở trình độ phát triển cao hơn về khoa học và xã hội, tư duy về lịch sử từ thế kỷ 17 
đến thế kỷ 19 có xu hướng chia rẽ thành hai cực đối lập, như đã từng diễn ra ở Hy 
Lạp, giữa trường phái duy tâm và duy vật. Cả hai trường phái tư tưởng đều vận 
động bởi cùng một mục tiêu. Họ tin rằng lịch sử có đặc điểm là có thể giải thích được 
và rằng có thể làm sáng tỏ bản chất và nguồn gốc của những quy luật lịch sử.

Những người diễn giải lịch sử theo quan điểm thần luận, như Bishop Bossuet
\index{Bishop Bossuet}, tiếp tục cho rằng Thượng đế dẫn dắt tiến trình lịch sử. 
Dù hầu hết các nhà tư tưởng khác không tranh cãi việc xét đến cùng thì thượng đế 
định hình tiến trình của các sự kiện, họ quan tâm hơn đến sự vận động lịch sử ở 
ngay trần thế.

Giambattista Vico\index{Vico} xứ Naples là người tiên phong vĩ đại trong số các triết gia. 
Ông khẳng định vào đầu thế kỷ 18 rằng do lịch sử, hay "các dân tộc trên thế giới," là 
sản phẩm của con người, có thể giải thích được lịch sử bằng chính những con người 
làm lên lịch sử. Ông nhấn mạnh các hiện tượng văn hóa và xã hội lần lượt trải qua 
các giai đoạn có tính chu kỳ. 

Ông tin rằng "trật tự của tư duy phải tuân theo trật tự của tự nhiên", và rằng 
"trật tự thuộc về nhân loại" đầu tiên là rừng, sau đó là những túp lều, rồi đến 
làng mạc, tiếp theo là thành thị và cuối cùng là trường học. "Khoa học mới" 
về lịch sử của ông là nhằm khám phá và vận dụng "những nguyên lý trường tồn và 
phổ quát ... mà theo đó mọi dân tộc được hình thành, và duy trì sự tồn tại." 
Vico đề xướng đấu tranh giai cấp trong cách diễn giải lịch sử của ông, đặc biệt 
trong thời kỳ anh hùng thể hiện xung đột giữa người dân và tầng lớp quý tộc 
ở Roma cổ đại.

Sau Vico\index{Vico}, những nhà duy vật ở Tây Âu, theo những hướng khác nhau, tìm kiếm 
"những nguyên lý trường tồn và phổ quát" quyết định lịch sử thế giới. Nhưng 
không có trường phái nào nghi ngờ rằng lịch sử, cũng như tự nhiên, tuân theo những 
quy luật tổng quát mà các triết gia nghiên cứu lịch sử có nhiệm vụ phải tìm ra. 

Tư tưởng chủ đạo của các nhà duy vật Anh, Pháp ở thế kỷ 17 và 18 cho rằng con 
người là sản phẩm của môi trường tự nhiên và xã hội. Như tiểu thuyết gia người Mỹ 
ở thế kỷ 19, Charles Brockden\index{Charles Brockden}, nói: "Con người được định 
hình bởi hoàn cảnh mà họ sinh sống." Theo nguyên lý này, họ lấy thực tại khách 
quan của thế giới tự nhiên và của xã hội để giải thích quá trình lịch sử. 

Montesquieu\index{Montesquieu}, chẳng hạn, coi địa lý tự nhiên và chính phủ là 
hai nhân tố quyết định của lịch sử và xã hội. Nhân tố tự nhiên có ảnh hưởng nhất ở 
những giai đoạn đầu, giai đoạn sơ khai hơn của nhân loại, mặc dù tác động của nó 
chưa bao giờ dừng lại; nhân tố chính trị ngày càng chiếm vai trò thống trị khi 
nền văn minh tiến lên phía trước. 

Ông ta và những người bạn theo trường phái duy vật cùng thời bỏ qua điều kiện 
kinh tế, cái đứng giữa tự nhiên và các tổ chức chính trị. Nền tảng kinh tế của 
các hệ thống chính trị và cuộc đấu tranh giữa các giai cấp đối lập phát sinh từ 
những mâu thuẫn kinh tế bị bỏ ra ngoài tầm nhìn của họ. 

Các nhà sử học người Pháp ở thế kỷ 19 đạt được nhiều hiểu biết sâu sắc về điều kiện 
kinh tế trong tiến trình lịch sử thông qua việc nghiên cứu những cuộc cách mạng 
ở Pháp và ở Anh. Họ chứng kiến Cách mạng Pháp\index{Cách mạng Pháp} trải qua một 
chu kỳ khép kín bắt đầu bằng sự lật đổ chế độ quân chủ chuyên chế, trải qua chế độ 
cách mạng của Rosbespierre\index{Rosbespierre} rồi đến chế độ độc tài quân-sự-tư-sản 
của Napoleon\index{Napoleon}, và cuối cùng là Khôi phục vương triều Bourbon\index{Bourbon}. 
Dưới ánh sáng của những thăng trầm cách mạng họ rút ra bài học về vai trò quyết 
định của đấu tranh giai cấp trong quá trình thúc đẩy lịch sử tiến lên, và họ chỉ 
ra rằng sự dịch chuyển chóng vánh của quan hệ sở hữu tài sản khi động cơ chủ đạo 
của xã hội đảo ngược. Thế nhưng, họ vẫn không thể tìm ra nhân tố quyết định để xây 
dựng lại và thay thế quan hệ sở hữu cũng như hình thức chính trị.

Nhiều nhà triết học triết học tiên phong của giai đoạn tư sản có quan điểm duy vật 
về tự nhiên và về mối quan hệ của con người với thế giới chung quanh. Nhưng không một
ai trong số họ đề ra được một quan niệm hoàn chỉnh và nhất quát về lịch sử và xã hội 
theo quan điểm duy vật. Nhiều lúc trong phân tích của họ, họ xa rời quan điểm 
và phương pháp duy vật, quy kết tác nhân cuối cùng cho hoạt động của con người vào 
bản chất con người, vào nguyên nhân con người sâu xa, hoặc vào những cá nhân vĩ đại. 

Điều gì đã khiến họ sa vào quan điểm không duy vật khi giải thích yếu tố quyết định 
của lịch sử và xã hội? Là những nhà triết học tư sản, họ bị giới hạn trong đường 
chân trời tư bản. Chừng nào giai cấp tư sản đang lên còn trên quá trình tiến tới 
thống trị, những đại diện xuất sắc cho ý thức hệ của nó hăng say và lúc nào cũng 
để tâm đào sâu vào thực tại kinh tế, chính trị, và xã hội. Sau khi giai cấp
tư sản đã củng cố vị trí của mình với tư cách là giai cấp thống trị, những nhà lý
luận cho ý thức hệ của nó co rúm khỏi việc phải tìm tòi đến tận cùng của quá 
trình chính trị và xã hội. Họ trở nên uể oải và thiển cận trong lĩnh vực xã hội 
học và lĩnh vực lịch sử  vì khám phá ra nguyên nhân dẫn đến sự thay đổi chỉ có thể 
là mối đe dọa sự tiếp nối của thống trị tư bản. 

Rào cản của những nghiên cứu nghiêm túc về khoa học xã hội là sự công nhận ngầm rằng 
xã hội tư sản và các thiết chế của nó là hình thức tổ chức xã hội cao nhất có thể đạt 
được. Tất cả xã hội trước đây đều tiến đến điểm đích đó, rồi kết thúc ở đó. Không có một 
lối ra tiến bộ rõ ràng nào để thoát khỏi hệ thống tư bản. Đó là lý do tại sao các 
nhà lý luận tư sản Anh như Locke\index{Locke} đến Ricardo\index{Ricardo} và 
Spencer\index{Spencer} tìm cách làm cho quan niệm lý giải ý nghĩa của mọi hiện 
tượng xã hội ăn khớp với mọi quan hệ và phạm trù của trật tự trong giai đoạn 
chuyển tiếp ấy. Sự hẹp hòi này cũng làm khó chính bản thân họ trong quá trình họ 
tìm cách giải thích quá khứ, nghiên cứu tới tận cùng về hiện tại, hay dự đoán về tương lai.

Giải thích duy tâm về lịch sử được thúc đẩy bởi một số nhà lý luận từ Leibnitz\index{Leibnitz} 
tới Fichte\index{Fichte}. Các công trình của họ đạt tới sự hoàn bị bởi Hegel\index{Hegel}. 
Vào đầu thế kỷ 19, Hegel\index{Hegel} tạo ra cuộc cách mạng trong hiểu biết về lịch sử 
thế giới; ông là người có tầm nhìn lịch sử sâu rộng nhất của thời kỳ tư sản. 
Có thể tóm tắt đóng góp của ông như sau:

1. Hegel\index{Hegel} tiếp cận mọi hiện tượng lịch sử từ quan điểm tiến hóa, xem chúng là những 
thời điểm, những thành phần, những giai đoạn trong một quá trình sáng tạo, tích lũy, 
phát triển, và không ngừng biến đổi và phát sinh.  
2. Bởi vì thế giới đối với ông, ông gọi là "Tư duy khách quan", là sản phẩm của con 
người, nên ông, giống như Vico, bị thuyết phục rằng thế giới ấy có thể được giải 
thích bằng tư duy.  
3. Ông nhận thức rằng lịch sử là một quá trình _phổ quát_ mà trong đó mọi hình 
thức xã hội, dân tộc và các cá nhân chiếm một chỗ đứng thích hợp nhưng chỉ là thứ 
yếu. Không có một nhà nước hay cá nhân đơn lẻ nào thống trị lịch sử thế giới; 
vai trò của nó phải được phán quyết trong sự phát triển có tính tổng thể.  
4. Ông khẳng định quá trình lịch sử về căn bản là một quá trình có lý trí. Nó chứa 
đựng logic nội tại diễn ra dưới những cách thức bị chi phối bởi quy luật xác lập
từ quá trình biện chứng.  
5. Mỗi thành phần căn bản của từng giai đoạn lịch sử được móc nối với nhau như 
những bộ phận của một tổng thể thống nhất bộc lộ nguyên lý thống trị của thời đại. 
Mỗi giai đoạn có đóng góp của riêng nó vào sự tiến bộ của nhân loại.  
6. Sự thật lịch sử có tính cụ thể. Như nhà tư tưởng người Nga, Chernyshevsky\index{Chernyshevsky},
viết: "Mỗi đối tượng, mỗi hiện tượng đều có ý nghĩa của riêng nó, và nó phải được 
phán quyết trong hoàn cảnh, trong môi trường mà nó tồn tại.... Một phán quyết 
xác định chỉ có thể tuyên bố về một sự việc xác định, sau khi đã tính đến mọi hoàn 
cảnh mà nó phụ thuộc."  
7. Lịch sử biến động một cách biện chứng. Mỗi giai đoạn phát triển xã hội phải 
có đủ điều kiện để trở thành hiện thực. Nó bao gồm những cấu phần mâu thuẫn, nảy 
sinh từ 3 yếu tố khác nhau. Đó là những thành tựu đạt được từ giai đoạn trước nó, 
những điều kiện cụ thể cần thiết để duy trì nó, và những lực lượng đối lập đang 
diễn ra bên trong nó. Sự phát triển của những đối kháng nội tại bên trong nó 
mang lại động lực và thúc đẩy nó tiến triển. Sự gia tăng mâu thuẫn dẫn đến 
sự tan rã và rốt cuộc là sự thay thế nó bằng một hình thức cao hơn và đối lập được 
nảy sinh thông qua bước nhảy vọt cách mạng.  
8. Như vậy là, mọi cấp độ của tổ chức xã hội liên kết với nhau thành một chuỗi được 
thiết lập một cách biện chứng từ thấp đến cao.   
9. Hegel đề xướng chân lý căn bản mà sau này được phát triển bởi chủ nghĩa duy vật 
lịch sử rằng lao động áp đặt lên con người là kết quả của nhu cầu và rằng con 
người là sản phẩm lịch sử của lao động.  
10. Lịch sử đầy rẫy những trớ trêu. Nó chứ đựng logic khách quan khiến những tổ 
chức và những cá nhân tham dự quyền năng nhất cũng phải bối rối. Mặc dù những người 
đứng đầu nhà nước áp đặt chính sách, còn người dân và những cá nhân theo đuổi những 
mục đích của riêng mình, thực tế lịch sử không tuân theo kế hoạch của họ. Diễn biến 
và kết cục lịch sử được quyết định bởi những tất yếu nội tại độc lập với ý chí 
và ý thức của mọi tổ chức và cá nhân. Con người dự tính ... tất yếu lịch sử mà 
Tinh Thần sắp đặt.   
11. Kết cục lịch sử là sự tiến triển của sự tự do có lý trí. Tự do của con người 
không phải đến từ việc can thiệp vào các sự kiện một cách duy ý chí và tùy tiện, 
mà là từ những phát triển bên trong tất yếu khách quan, một quá trình phát sinh 
từ mâu thuẫn phổ quát.   
12. Tất yếu lịch sử không phải bao giờ cũng như nhau; nó biến đổi thành cái đối 
lập khi giai đoạn này thay thế giai đoạn khác. Trong thực tế, sự mâu thuẫn giữa 
tất yếu ở mức thấp hơn với tất yếu ở mức cao hơn là nguồn gốc sản sinh ra sự tiến bộ. 
Cái tất yếu đang lớn mạnh, vận động bên trong trật tự đang tồn tại, phủ định 
những điều kiện duy trì nó. Tất yếu đang lớn mạnh này cứ tiếp tục tước đoạt tất 
yếu hiện tại khỏi lý do tồn tại của nó, bành trướng buộc nó phải trả giá, làm 
cho nó trở thành cũ kỹ và cuối cùng thì thay thế nó.   
13. Không chỉ có hình thức xã hội và nguyên lý thống trị của những hình thức ấy 
thay đổi từ giai đoạn này sang giai đoạn tiếp theo, mà cả những quy luật đặc thù 
của sự phát triển cũng thay đổi. 

Phương pháp giải thích lịch sử này đúng đắn, hoàn thiện và sâu sắc hơn nhiều 
so với mọi phương pháp trước đó. Nhưng nó chứa đựng hai sai lầm không thể khắc 
phục được. Đầu tiên là quan điểm duy tâm không thể sửa chữa. Hegel\index{Hegel} 
xem lịch sử là một sản phẩm của những nguyên lý trừu tượng thể hiện ở những cấp độ khác 
nhau trong cuộc đấu tranh không ngừng giữa nô lệ và tự do. Tự do của con người 
dần sẽ thành hiện thực thông qua quá trình phát triển biện chứng của 
Tinh Thần Tuyệt Đối\index{Tinh Thần Tuyệt Đối}. 

Logic về lịch sử như vậy là phiên bản trí tuệ hoá của quan niệm Thượng Đế dẫn dắt 
vũ trụ và lịch sử là sự hoàn chỉnh bản thiết kế của Thượng Đế, mà trong trường hợp 
này thiết kế đó là tự do của nhân loại. Theo như Hegel\index{Hegel} dự tính, 
tự do không trở thành hiện thực thông qua giải phóng nhân loại khỏi những điều 
kiện áp bức mà thông qua việc đánh bại những ý tưởng sai lầm và không phù hợp.

Thứ hai là Hegel\index{Hegel} đã đóng cánh cổng dẫn đến sự phát triển tiếp theo 
của lịch sử bằng cách cho rằng lịch sử đã lên tới đỉnh cao nhất là vương quốc Đức 
và xã hội tư sản ở thời đại của ông. Người diễn giải về sự phát triển của lịch sử
phổ quát và không có điểm kết thúc lại đi đến kết luận rằng tác nhân tột cùng là 
nhà nước dân tộc, một sản phẩm đặc trưng của giai đoạn tư sản. Và bằng hình thức 
quân chủ, có điều chỉnh bằng hiến pháp! Ông nhầm lẫn thể hiện có tính chất chuyển 
tiếp của lịch sử sang một thể hiện cuối cùng và hoàn hảo. Bằng cách đặt giới hạn 
cho quá trình phát triển, ông vi phạm nguyên lý căn bản trong phép biện chứng 
của chính mình. 

Những khiếm khuyết này ngăn cản Hegel\index{Hegel} đi tới bản chất thực sự của 
quan hệ xã hội và những nguyên lý làm thay đổi xã hội. Nhưng hiểu biết sâu sắc 
có tính đánh dấu thời đại của ông đã ảnh hưởng tới tất cả tư tưỏng và nghiên cứu 
sau này về lịch sử. Với những khắc phục không thể thiếu, những hiểu biết sâu sắc 
ấy được kết hợp vào cấu trúc của chủ nghĩa duy vật lịch sử.

Hegel\index{Hegel}, theo trường phái biện chứng duy tâm\index{biện chứng duy tâm}, 
là nhà tư tưởng lỗi lạc nhất về quá trình tiến hóa. Các nhà tư tưởng xã hội và 
lịch sử người Pháp đưa giải thích duy vật về lịch sử và xã hội đến bến bờ xa nhất 
có thể trong thời đại của mình. Nhưng ngay trong địa hạt của riêng mình cả giải 
thích về lịch sử và xã hội của họ đều không đi tới đích. Hegel\index{Hegel} không 
thể đưa ra một lý thuyết thỏa đáng về tiến hóa xã hội, còn các nhà duy vật thì 
không thể xâm nhập được vào lực lượng vận động cơ bản của lịch sử.

Phải cho đến khi những yếu tố đúng đắn của hai luồng tư tưởng đối lập này hội tụ 
vào tư tưởng của Marx\index{Marx} và Engels\index{Engels} vào giữa thế kỷ 19 
thành quan niệm lịch sử là sản phẩm định hình bởi sự phát triển biện chứng của 
điều kiện vật chất của tồn tại xã hội từ thời kỳ nảy sinh loài người cho đến thời 
kỳ đương đại.

Tất cả mọi cách thức giải thích lịch sử khác nhau được tích lũy trong tiến hóa của 
tư tưởng nhân loại và vẫn sống sót đến ngày hôm nay. Không có cách giải thích nào 
bị chôn vùi vĩnh viễn, cho dù nó có lạc hậu, không thỏa đáng hay không khoa học 
đến mức nào đi chăng nữa. Cách giải thích cũ kĩ nhất cũng có thể được hồi sinh và 
lại xuất hiện dưới vỏ bọc hiện đại để phục vụ một nhu cầu xã hội hoặc một giai tầng 
nào đó. 

Có nhà nước tư sản nào trong thời chiến lại không tuyên bố "Thượng Đế ở phe của 
chúng ta"? Thuyết Vĩ Nhân oai vệ bên dưới chữ thập ngoặc. Ở Đức là 
Spengler\index{Spengler} và ở Anh là Toynbee\index{Toynbee} đề xướng phiên bản của 
riêng họ về chu kỳ lặp lại của lịch sử. Trường phái địa chính trị đưa điều kiện 
địa lý thành cái quyết định tột cùng của lịch sử hiện đại. 

Đức Quốc xã\index{Đức Quốc xã}, Verwoerd\index{Verwoerd} ở Nam Phi và những người 
theo chủ nghĩa da trắng thượng đẳng ở Miền Nam ca ngợi chủng tộc thống trị có vai 
trò như kẻ độc tài của lịch sử. Quan niệm rằng bản chất con người phải là nền tảng 
của cấu trúc xã hội là chốt phòng thủ cuối cùng của những người phản đối chủ nghĩa 
xã hội, nó cũng là điểm xuất phát cho chủ nghĩa xã hội không tưởng ở nhà phân 
tâm học Erich Fromm\index{Erich Fromm} và những người khác.

Và cuối cùng, quan niệm rằng lý trí là động lực thúc đẩy của lịch sử được mọi 
cá nhân có học vấn ở đủ loại chuyên ngành đóng góp. Nhà nhân loại học người Mỹ 
Alexander Goldenweiser\index{Alexander Goldenweiser} tuyên bố trong 
_Nền Văn minh sớm_ (_Early Civilization_): "Toàn bộ nền văn minh, nếu cứ đi ngược 
trở lại từng bước một, cuối cùng sẽ tìm thấy, mà không có dư sai nào, những mẩu 
tư tưởng trong tư duy của những cá nhân." Ở đây tư tưởng và cá nhân là nhân tố 
tạo nên lịch sử. 

Khi mô tả triết học của mình, nhà tư tưởng người Ý, Croce\index{Croce} viết: 
"Lịch sử là ghi ghép về những sáng tạo của tinh thần nhân loại trong mọi lĩnh vực, 
từ lý thuyết đến thực hành. Những sáng tạo tinh thần này bao giờ cũng được sinh 
ra từ trái tim và khối óc của những thiên tài, những nghệ sĩ, nhà tư tưởng, những nhà 
hoạt động, những nhà cải cách đạo đức và tôn giáo." Quan điểm này kết hợp chủ 
nghĩa duy tâm với chủ nghĩa thượng đẳng, tinh thần của những thiên tài, hoặc của 
một thiểu số có tính sáng tạo, có vai trò như những tác nhân cứu rỗi đám đông. 

Những yếu tố đa dạng trong giải thích lịch sử này có thể xuất hiện trong sự kết hợp 
phi lý nhất ở một đất nước, ở một trường phái tư tưởng hoặc ở một cá nhân. Chủ 
nghĩa Stalin\index{Stalin} là một ví dụ sống động nhất về sự tổng hợp phi logic ấy. 
Những kẻ tôn thờ "sùng bái cá nhân" tìm cách hợp nhất truyền thống và thế giới 
quan Marxist, một triết học khoa học và hiện đại nhất, với phiên bản cổ hủ của 
thuyết Vĩ Nhân trong tiến trình lịch sử đương đại. 

Ngoại trừ ở Trung Quốc\index{Trung Quốc} dưới thời Mao\index{Mao}, mớ hỗn độn 
của những tư tưởng kỳ quặc và không thể trụ vững được đã sụp đổ tan tành. Nhưng 
nó cho thấy làm thế nào mà tư tưởng khái quát về quá trình lịch sử có thể đi ngược 
trở lại sau khi nó đã thực hiện những bước nhảy vĩ đại về phía trước. Lịch sử của 
khoa học lịch sử chứng minh bằng cách riêng của nó rằng sự tiến bộ là không bằng
phẳng và không liên tục trong suốt chiều dài lịch sử. Thucydides\index{Thucydides}, 
người kể chuyện lịch sử Chiến Tranh Peloponnesian vào thế kỷ thứ 4 TCN, có thế giới
quan về lịch sử thực tế hơn nhiều so với St. Augustine\index{St. Augustine}, 
người ca ngợi _Thành phố của Thượng Đế_\index{Thành phố của Thượng Đế} 
(_the City of God_) ở thế kỷ thứ 4.

Chủ nghĩa duy vật lịch sử loại bỏ Người Dẫn dắt Thần thánh, Vĩ Nhân, Tinh thần 
Vũ trụ, Thiên tài Trí tuệ, Giới Tinh hoa, và Bản chất Con người không thể thay 
đổi khỏi cách giải thích lịch sử. Sự hình thành, tái hình thành và biến đổi của 
các cấu trúc xã hội qua hàng triệu năm không thể giải thích bằng cách dựa vào bất
cứ tồn tại siêu nhiên nào, một tác nhân lý tưởng nào, một nguyên nhân bất biến 
hay cá nhân nhỏ mọn nào. 

Thượng đế không tạo ra thế giới và cũng không giám sát sự phát triển của nhân loại.
Trái lại, con người sáng tạo ra ý tưởng về thượng đế như một điều kỳ diệu để bù 
đắp việc họ thiếu khả năng chế ngự được sức mạnh trong tự nhiên và trong xã hội. 
Con người làm nên chính mình khi nó tác động lên thế giới tự nhiên và biến đổi 
các yếu tố tự nhiên để thỏa mãn nhu cầu của bản thân thông qua quá trình lao động.
Con người đã làm như vậy trong suốt quá trình tồn tại. Sự phát triển xa hơn 
và sự đa dạng hóa của quá trình lao động từ thời kỳ man rợ đến nên văn minh
hiện tại lại tiếp tục làm biến đổi khả năng và tính cách của con người. 

Lịch sử không phải là thành tựu của những cá nhân xuất chúng, cho dù họ có toàn 
năng, tài giỏi hay thao lược đến mức nào đi chăng nữa. Ngay thời kỳ đầu của 
Cách mạng Pháp\index{Cách mạng Pháp}, Condorcet\index{Condorcet} đã phản đối 
quan điểm đẳng cấp tinh hoa hẹp hòi, quan điểm ấy không đếm xỉa đến những gì đã 
vận động quần chúng và không tính đến việc làm thế nào mà quần chúng chứ không 
phải những ông chủ làm nên lịch sử. Ông viết "Cho đến giờ, lịch sử chính trị, 
cũng như lịch sử triết học hay lịch sử khoa học, chỉ là lịch sử của một vài cá nhân: 
Những gì thực sự tạo nên nhân loại, quần chúng nhân dân đông đảo trong những gia 
đình sống trên thành quả lao động của chính họ, đều bị quên lãng, và thậm chí những 
người làm công bộc cho nhà nước, họ không làm việc cho bản thân họ mà cho xã hội, 
những người tham gia dạy học, cai quản, bảo vệ và chăm sóc những người khác, 
nhưng chỉ thấy có lãnh đạo là được các nhà sử học để mắt tới." 

Chủ nghĩa Marx\index{Marx} xây dựng trên hiểu biết sâu sắc rằng lịch sử là kết quả 
từ hoạt động có tính tập thể diễn ra ở nhiều cấp độ, từ nỗ lực của quần chúng trải 
qua những giai đoạn lâu dài bên trong bộ khung của lực lượng sản xuất mà họ đã 
tiếp nhận, rồi mở rộng và hình thái sản xuất mà họ đã tạo ra, xây dựng và cách mạng hóa. 
Không phải giới tinh hoa mà là tập thể gồm nhiều thành viên đã duy trì lịch sử, 
xoay chuyển nó theo những hướng đi mới khi nó đạt tới bước ngoặt và thúc đẩy nhân 
loại từng bước tiến về phía trước.

Lịch sử không phải được phát sinh, tiến trình của nó cũng không phải được dẫn dắt bởi 
những tư tưởng được thai nghén từ trước. Những hệ thống xã hội không được dựng 
lên bởi những kiến trúc sư với bản vẽ trong tay. Lịch sử không tuân theo bất cứ 
một kế hoạch nào. Các hình thức kinh tế xã hội được sản sinh từ lực lượng sản xuất; 
thành viên của lực lượng ấy tạo ra những mối quan hệ, tập quán, thiết chế và tư 
tưởng tương xứng với tổ chức lao động của họ.

Cái gọi là bản chất con người không thể giải thích tiến trình của các sự kiện 
hoặc đặc tính của đời sống xã hội. Những thay đổi của điều kiện sinh sống và lao 
động là nền tảng để tạo ra và tái tạo ra bản chất con người. 

Trong lời giới thiệu bản tiếng Anh của tác phẩm "Sự phát triển của chủ nghĩa xã 
hội: từ không tưởng đến khoa học", Engels\index{Engels} định nghĩa chủ nghĩa 
duy vật biện chứng là "quan điểm về tiến trình lịch sử tìm kiếm nguyên nhân tận 
cùng và động lực vận động của mọi sự kiện lịch sử ở bên trong sự phát triển kinh 
tế xã hội, trong phương thức sản xuất và trao đổi, trong sự phân chia xã hội 
thành các giai cấp khác biệt, và trong cuộc đấu tranh giữa các giai cấp ấy."

Đó là những nguyên lý mà học thuyết Marx\index{Marx} về tiến trình lịch sử được phát triển. 
Những nguyên lý ấy là kết quả của cuộc tìm kiếm trong hai thiên niên kỷ rưỡi những 
quy luật về hoạt động của con người và của sự phát triển xã hội. Nó đi đến kết luận 
đúng đắn. Duy vật lịch sử bản thân nó là sản phẩm tổng hợp của sự kiện lịch sử và
của tư tưởng có gốc rễ từ kinh tế và là thành quả của khoa học xã hội.

