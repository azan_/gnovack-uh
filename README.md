## Dự án dịch các bài viết về chủ đề lịch sử của George Novack sang tiếng Việt 

## Giới thiệu về George Novack 

https://www.marxists.org/archive/novack/works/history/intro.htm

## George Novack’s Understanding History

[1] https://www.marxists.org/archive/novack/works/history/index.htm

## Tình trạng

- [x] Major Theories Of History From The Greeks To Marxism  
- [x] From Lenin To Castro 
- [ ] The Long View Of History (planned) 

